﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="OTTValidation.aspx.vb" Inherits="OTTValidation" Debug="true"  %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Welcome</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="stylesheet" href="assets/demo.css" />
    <link rel="stylesheet" href="assets/form-labels-on-top.css" />
    <link href="assets/Register_style.css" rel="stylesheet" />

    <style>
        .invalid-data {
            display: block;
            text-align: right;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" class="form-labels-on-top">

        <div id="main-content">
            <div><img src="image/unifiLogo.png" width="77" height="83"/></div>
            <div class="form-title-row">
                <h1>Account Validation</h1>
            </div>

            <div class="form-row">
                <label>
                    <span>unifi or Streamyx* Account ID</span>
                    <asp:TextBox ID="TM_AccountID" runat="server" placeholder="Account ID" ReadOnly="True"></asp:TextBox>
                </label><br />
                <asp:Label ID="LabelMsgAccountID" runat="server" CssClass="smalltext"></asp:Label>
                <br />
                
            </div>

            <div class="form-row" id="dropdownIdTypeView" runat="server">
                <label>
                    <span>Identification</span>
                        <asp:DropDownList ID="ddListIdType" AutoPostBack="True" OnSelectedIndexChanged="dropDownListIdType_changed" AppendDataBoundItems="False" runat="server">
                    </asp:DropDownList>
                </label>
            </div>

            <div class="form-row" id="IdFieldView" runat="server">
                <asp:TextBox ID="keyinID" runat="server" placeholder="Key In Identification"></asp:TextBox>
                <br />
                <asp:Label ID="lblMsgId" runat="server" CssClass="smalltext"></asp:Label>                
            </div>

            <div class="form-row">
                <asp:Button ID="Submit" runat="server" Text="Submit" />
                <asp:Button ID="Cancel" runat="server" Text="Cancel" />
                <asp:Label ID="lblMsgProcess" runat="server" CssClass="smalltext"></asp:Label>
            </div>
        </div>

       <%--<asp:HiddenField ID="OTID" runat="server"/>
       <asp:HiddenField ID="TXNID" runat="server"/>
        <div id="main-content">
            <div class="form-title-row">
                <h1>Account Validation</h1>
            </div>

            <div class="form-row">
                <label>
                    <span>UniFi or Streamyx* Account ID</span>
                    <asp:TextBox ID="TM_AccountID" runat="server" placeholder="Account ID"></asp:TextBox>
                </label><br />
                <asp:Label ID="LabelMsgAccountID" runat="server" CssClass="smalltext"></asp:Label>
            </div>

            <div class="form-row" id="dropdownIdTypeView" runat="server">
                <label>
                    <span>Identification</span>
                    <asp:DropDownList ID="ddListIdType" AutoPostBack="True" OnSelectedIndexChanged="dropDownListIdType_changed" AppendDataBoundItems="False" runat="server">
                    </asp:DropDownList>
                </label>
            </div>
            
            <div id="IdFieldView_1" runat="server">
                <label id="IdOne">
                    <asp:TextBox ID="id_1" runat="server" Columns="6" MaxLength="6"></asp:TextBox>
                    <asp:Label ID="dash_1" runat="server" CssClass="smalltext-white">-</asp:Label>
                    <asp:TextBox ID="id_2" runat="server" Columns ="2" MaxLength="2"></asp:TextBox>
                    <asp:Label ID="dash_2" runat="server" CssClass="smalltext-white">-</asp:Label>
                    <asp:TextBox ID="id_3" runat="server" Columns ="4" MaxLength="4"></asp:TextBox>
                    <asp:TextBox ID="keyinID" runat="server" placeholder="Key In Identification"></asp:TextBox>
                </label><br />
                <asp:Label ID="lblMsgId" runat="server" CssClass="smalltext"></asp:Label>
            </div>

            <div class="form-row">
                <asp:Button ID="Submit" runat="server" Text="Submit" />
                <asp:Button ID="Cancel" runat="server" Text="Cancel" />
                <asp:Label ID="lblMsgProcess" runat="server" CssClass="smalltext"></asp:Label>
            </div>
        </div>--%>
    </form>
</body>
</html>
