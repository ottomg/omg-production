﻿Imports Microsoft.VisualBasic
Imports System.Xml.Serialization

Public Class TMBilling_Object

End Class

Public Class INT_APILog
    Public TM_AccountID As String
    Public TMProductID As Integer
    Public OMGProductID As Integer
    Public EventName As String
    Public EventURL As String
    Public DataReq As String
    Public DataResp As String
    Public ProcessStatus As String
End Class

Public Class TMProductDetails
    Public TMMerchantID As String
    Public TMProductID As Integer
    Public TMProductCode As String
    Public TMProductName As String
    Public ProductType As String
    Public OMGProductID As Integer
    Public OMGProductCode As String
    Public OMGProductName As String
    Public OTTMerchantID As String
    Public Status As String
    Public ResponseCode As String
    Public ResponseMsg As String
End Class

'OMG ============================
<XmlRoot("OMG_ValidateUserReq")>
Public Class OMG_ValidateUserReq
    Public OTT_ID As String
    Public TM_AccountID As String
    Public Id As String
    Public IdType As String
    Public ClientType As String     'STB or WEB
    Public AccountType As String   'Unifi or Streamyx
    Public Signature As String
End Class

<XmlRoot("OMG_ValidateUserResp")>
Public Class OMG_ValidateUserResp

    Public TM_AccountID As String
    Public STBId As String

    Public TM_AccountType As String
    Public MobileNumber As String
    Public CustomerEmail As String
    Public ServiceNumber As String
    Public BundleName As String
    Public ProductName As String
    Public FullName As String

    Public Id As String
    Public IdType As String
    Public OMG_TXNID As String
    <XmlElement("Address", IsNullable:=False)>
    Public Address As Address

    Public BillingAccountNo As String
    Public Race As String
    Public ExchangeId As String

    Public ResponseCode As String
    Public ResponseMsg As String
End Class

Public Class Address
    Public HouseNo As String
    Public FloorNo As String
    Public BuildingName As String
    Public StreetType As String
    Public StreetName As String
    Public Section As String
    Public PostalCode As String
    Public City As String
    Public StateName As String
    Public Country As String
End Class

<XmlRoot("INT_ValidateUserReq")>
Public Class INT_ValidateUserReq
    Public TM_AccountID As String
    Public TM_AccountType As String
End Class

<XmlRoot("INT_ValidateUserResp")>
Public Class INT_ValidateUserResp

    Public TMAccountID As String
    Public STBId As String

    Public Status As String
    Public TM_AccountType As String
    Public MobileNumber As String
    Public CustomerEmail As String
    Public TM_BillCycle As String
    Public Id As String
    Public IdType As String

    Public AccountName As String
    Public HouseNo As String
    Public FloorNo As String
    Public BuildingName As String
    Public StreetType As String
    Public StreetName As String
    Public Section As String
    Public PostalCode As String
    Public City As String
    Public StateName As String
    Public Country As String
    Public HomePhone As String
    Public BundleName As String
    Public ProductName As String
    Public AssetStatus As String
    Public ServiceNumber As String

    Public BillingAccountNo As String
    Public OwnerAccountId As String
    Public Race As String
    Public ExchangeId As String

    Public ResponseCode As String
    Public ResponseMsg As String
End Class

'NOVA / ICP ============================
<XmlRoot("ValidateAccountRequest")>
Public Class ValidateAccountRequest
    Public EventName As String
    Public TM_AccountID As String
End Class

<XmlRoot("ValidateAccountResponse")>
Public Class ValidateAccountResponse

    Public TMAccountID As String
    Public STBId As String

    Public Status As String
    Public MobileNumber As String
    Public CustomerEmail As String
    Public TM_BillCycle As String
    Public Id As String
    Public IdType As String

    Public AccountName As String
    Public HouseNo As String
    Public FloorNo As String
    Public BuildingName As String
    Public StreetType As String
    Public StreetName As String
    Public Section As String
    Public PostalCode As String
    Public City As String
    Public StateName As String
    Public Country As String
    Public HomePhone As String
    Public BundleName As String
    Public ProductName As String
    Public AssetStatus As String
    Public ServiceNumber As String

    Public BillingAccountNo As String
    Public OwnerAccountId As String
    Public Race As String
    Public ExchangeId As String
    <XmlElement("AccountIntegration")>
    Public AccountIntegration() As AccountIntegration

    Public ResponseCode As String
    Public ResponseMsg As String
End Class

Public Class AccountIntegration
    Public Id As String
    Public LastName As String
    Public FirstName As String
    Public HomePhone As String
    Public CellularPhone As String
    Public WorkPhone As String
    Public FaxPhone As String
    Public EmailAddress As String
End Class

'OMG ============================
<XmlRoot("INT_NewOrderReq", IsNullable:=True)>
Public Class INT_NewOrderReq
    Public ReqType As String
    Public TM_AccountID As String
    Public TM_AccountType As String
    Public TM_MobileNo As String
    Public TM_Email As String
    Public TMProductID As Integer
    Public TMProductCode As String
    Public TMProductName As String
    Public ProductType As String
    Public ActivationDate As String
    Public DeactivationDate As String
End Class

<XmlRoot("INT_NewOrderResp")>
Public Class INT_NewOrderResp
    Public ResponseCode As String
    Public ResponseMsg As String
End Class

<XmlRoot("OMG_SubmitOrderRequest")>
Public Class OMG_SubmitOrderRequest
    Public OTT_ID As String
    Public TM_AccountID As String
    Public TM_AccountType As String
    Public TM_MobileNo As String
    Public TM_Email As String
    Public ProductId As String
    Public ProductName As String
    Public ActivationDate As String
    Public Signature As String
End Class

<XmlRoot("OMG_SubmitOrderResponse")>
Public Class OMG_SubmitOrderResponse
    Public ResponseCode As String
    Public ResponseMsg As String
    Public OTT_ActivationDate As String
End Class

'NOVA / ICP ============================
<XmlRoot("SubmitOrderRequest", IsNullable:=True)>
Public Class SubmitOrderRequest
    Public EventName As String
    Public TM_AccountID As String
    Public ActivationDate As String
    Public DeactivationDate As String
    Public ProductId As String
    Public ProductName As String
    Public OTCIndicator As String
    Public RemoveOTTFlag As String
End Class

Public Class INT_SubmitOrderResponse
    Public OrderNumber As String
    Public Status As String
    Public ProductId As String
    Public TM_AccountID As String
    Public ResponseCode As String
    Public ResponseMsg As String
End Class

<XmlRoot("OMG_SubmitTerminationRequest")>
Public Class OMG_SubmitTerminationRequest
    Public OTT_ID As String
    Public TM_BillingCycleDate As String
    Public TM_AccountID As String
    Public TM_AccountType As String
    Public ProductId As String
    Public ProductName As String
    Public Signature As String
End Class

<XmlRoot("OMG_SubmitTerminationResponse")>
Public Class OMG_SubmitTerminationResponse
    Public ResponseCode As String
    Public ResponseMsg As String
End Class

<XmlRoot("INT_SetTransactionIDReq")>
Public Class INT_SetTransactionIDReq
    Public TM_AccountID As String
    Public MobileNumber As String
    Public CustomerEmail As String
    Public Id As String
    Public IdType As String
    Public TM_BillCycle As String
    Public OTT_TXNID As String
    Public Token As String
End Class

<XmlRoot("INT_SetTransactionIDResp")>
Public Class INT_SetTransactionIDResp
    Public Status As String
    Public OMG_TXNID As String
    Public ResponseCode As String
    Public ResponseMsg As String
End Class

<XmlRoot("INT_GetTransactionIDReq")>
Public Class INT_GetTransactionIDReq
    Public OMG_TXNID As String
    Public TM_AccountID As String
End Class

<XmlRoot("INT_GetTransactionIDResp")>
Public Class INT_GetTransactionIDResp
    Public OMG_TXNID As String
    Public TM_AccountID As String
    Public MobileNumber As String
    Public CustomerEmail As String
    Public TM_BillCycle As String
    Public OTT_ID As String
    Public OTT_TXNID As String
    Public ProductID As String
    Public Status As String
    Public ResponseCode As String
    Public ResponseMsg As String
End Class

'OMG ============================
<XmlRoot("OMG_GetProductStatusReq")>
Public Class OMG_GetProductStatusReq
    Public OTT_ID As String
    Public SubscriberID As String
    <XmlElement("ProductList")>
    Public ProductList As ProductList
    Public Signature As String
End Class

<XmlRoot("OMG_GetProductStatusResp")>
Public Class OMG_GetProductStatusResp
    Public SubscriberID As String
    <XmlElement("ProductList")>
    Public ProductList As ProductList
    Public ResponseCode As String
    Public ResponseMsg As String
End Class

Public Class INT_SubscriptionStatusReq
    Public SubscriberID As String
    <XmlElement("ProductList")>
    Public ProductList As ProductList
End Class

'NOVA / ICP ============================
<XmlRoot("SubscriptionStatusRequest", IsNullable:=True)>
Public Class SubscriptionStatusRequest
    Public EventName As String
    Public ServiceID As String
    <XmlElement("ProductList")>
    Public ProductList As ProductList
End Class

Public Class ProductList
    Public Total As Integer
    <XmlElement("Product", IsNullable:=False)>
    Public Product() As Product
End Class

Public Class Product
    Public Id As String
    Public Status As String
End Class

Public Class SubscriptionStatusResponse
    Public EventName As String
    Public ServiceID As String
    <XmlElement("ProductList")>
    Public ProductList As ProductList
    Public ResponseCode As String
    Public ResponseMsg As String
End Class

'OMG ============================
<XmlRoot("OMG_RetryOTTOrderReq", IsNullable:=True)>
Public Class OMG_RetryOTTOrderReq
    Public OTT_ID As String
    Public TMBill_ID As Integer
    Public TM_AccountID As String
    Public ReqType As String
    Public Signature As String
End Class

<XmlRoot("OMG_RetryOTTOrderResp")>
Public Class OMG_RetryOTTOrderResp
    Public ResponseCode As String
    Public ResponseMsg As String
End Class

<XmlRoot("INT_OTTOrderReq")>
Public Class INT_OTTOrderReq
    Public TMBill_ID As Integer
    Public TM_AccountID As String
End Class

<XmlRoot("INT_OTTOrderResp")>
Public Class INT_OTTOrderResp
    Public TM_AccountID As String
    Public TM_AccountType As String
    Public TM_MobileNo As String
    Public TM_Email As String
    Public TM_BillCycle As String
    Public Status As String
    Public ActivationDate As String
    Public TMProductID As Integer
    Public TMProductCode As String
    Public TMProductName As String
    Public ProductType As String
    Public ResponseCode As String
    Public ResponseMsg As String
End Class

'For Teka Tekan ============================
<XmlRoot("AccountID")>
Public Class AccountID
    Public TM_AccountID As String
End Class

<XmlRoot("OMG_AccountDetailRequest")>
Public Class OMG_AccountDetailRequest
    Public OTT_ID As String
    Public showID As Integer
    Public AccountIDs() As AccountID
    Public Signature As String
End Class

<XmlRoot("OMG_AccountDetailResponse")>
Public Class OMG_AccountDetailResponse
    Public ResponseCode As String
    Public ResponseMsg As String
End Class

'<XmlRoot("OMG_SendSMSReq")> _
'Public Class OMG_SendSMSReq
'    Public TM_AccountID As String
'    Public MobileNumber As String
'    Public OrderNumber As String
'    Public ProductId As String
'End Class

'<XmlRoot("OMG_SendSMSResp")> _
'Public Class OMG_SendSMSResp
'    Public ResponseCode As String
'    Public ResponseMsg As String
'End Class

'<XmlRoot("OMG_SendNotifyReq")> _
'Public Class OMG_SendNotifyReq
'    Public OTT_ID As String
'    Public SendType As Integer
'    Public Email As String
'    Public MobileNumber As String
'    Public EmailSubject As String
'    Public MessageBody As String
'    Public Signature As String
'End Class

'<XmlRoot("OMG_SendNotifyResp")> _
'Public Class OMG_SendNotifyResp
'    Public ResponseCode As String
'    Public ResponseMsg As String
'End Class
