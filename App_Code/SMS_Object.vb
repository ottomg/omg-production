﻿Imports Microsoft.VisualBasic
Imports System.Xml.Serialization

Public Class SMS_Object

End Class

Public Class GeneralResponse
    Public ResponseCode As String
    Public ResponseMsg As String
End Class

<XmlRoot("OMG_SendSMSReq")>
Public Class OMG_SendSMSReq
    Public OTT_ID As String
    Public TM_AccountID As String
    Public MobileNumber As String
    Public ProductId As String
    Public SMSText As String
    Public RandomID As String
End Class

<XmlRoot("OMG_SendSMSResp")>
Public Class OMG_SendSMSResp
    Public ResponseCode As String
    Public ResponseMsg As String
End Class
