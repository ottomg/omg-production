﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography
Imports System.Net
Imports System.Xml
Imports System.Xml.Serialization
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System

Public Class OMG_Function

    Dim MyDb As New dbConn
    Dim strConn As New SqlConnection(MyDb.connString)

    Function GetEncryption(ByVal toHash As String, Optional ByVal ShaType256 As Boolean = False) As String

        Dim buffer As Byte() = Encoding.Default.GetBytes(toHash)
        Dim hash As String = ""

        If ShaType256 = True Then
            Dim cryptoTransformSha256 As New SHA256CryptoServiceProvider()
            hash = BitConverter.ToString(cryptoTransformSha256.ComputeHash(buffer)).Replace("-", "")
        Else
            Dim cryptoTransformSha1 As New SHA1CryptoServiceProvider()
            hash = BitConverter.ToString(cryptoTransformSha1.ComputeHash(buffer)).Replace("-", "")
        End If

        Return hash
    End Function

    Function GetSerializeXML(ByVal obj As Object, ByVal type As Type) As String

        Dim xmlDOc As New XmlDocument
        Dim sXML As String
        Dim xmlStream As New MemoryStream
        Dim xmlSerializer As XmlSerializer = New XmlSerializer(obj.GetType)
        Dim xmlTextWriter As XmlTextWriter = New XmlTextWriter(xmlStream, Encoding.UTF8)

        xmlSerializer.Serialize(xmlTextWriter, obj)
        xmlStream.Position = 0
        sXML = xmlStream.ToString
        xmlDOc.Load(xmlStream)

        Return xmlDOc.OuterXml
    End Function

    Function GetDeserializeXML(ByVal obj As String, ByVal type As Type) As Object

        Dim stringread As StringReader = New StringReader(obj)
        Dim xmlSerializer As XmlSerializer = New XmlSerializer(type)
        Dim xmlReader As XmlTextReader = New XmlTextReader(stringread)
        Dim anObject As New Object

        Try
            anObject = xmlSerializer.Deserialize(xmlReader)
        Catch ex As Exception

        Finally
            xmlReader.Close()
            stringread.Close()
        End Try

        Return anObject
    End Function

    Function RemoveSoapEnvelope(ByVal obj As String, ByVal searchString As String) As String

        Dim objRemove As String

        Dim startindex As Integer = obj.IndexOf(searchString)
        searchString = "</" + searchString.Substring(1)
        Dim endIndex As Integer = obj.IndexOf(searchString)
        objRemove = obj.Substring(startindex, endIndex + searchString.Length - startindex)

        Return objRemove
    End Function

    Function GenerateRandomString(ByVal length As Integer, Optional ByVal upper As Boolean = False) As String

        Dim xStr As String = String.Empty
        Dim StringGenerator As String = String.Empty

        If upper = True Then
            StringGenerator = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        Else
            StringGenerator = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLOMNOPQRSTUVWXYZ"
        End If

        Dim xCharArray() As Char = StringGenerator.ToCharArray
        Dim xGenerator As System.Random = New System.Random()

        While xStr.Length < length
            xStr &= xCharArray(xGenerator.Next(0, xCharArray.Length))
        End While

        Return xStr
    End Function

    Function GenerateRandomInteger() As Integer

        Dim xInt As Integer
        Dim xGenerator As System.Random = New System.Random
        xInt = xGenerator.Next(100000, 999999)

        Return xInt
    End Function

    Function HTTPPost(ByVal datatoPost As String, ByVal URLtoPost As String) As String

        If URLtoPost <> Nothing Then

            'Create a request using a URL that can receive a post. 
            Dim MyWebRequest As HttpWebRequest = WebRequest.Create(URLtoPost)
            ' Set the Method property of the request to POST.
            MyWebRequest.Method = "POST"

            'MyWebRequest.Headers.Add(URLtoPost)
            ' Create POST data and convert it to a byte array.
            Dim byteArray As Byte() = Encoding.UTF8.GetBytes(datatoPost)
            ' Set the ContentType property of the WebRequest.
            MyWebRequest.ContentType = "text/xml"
            ' Set the ContentLength property of the WebRequest.
            MyWebRequest.ContentLength = byteArray.Length

            Try
                ' Get the request stream.
                Dim dataStream As Stream = MyWebRequest.GetRequestStream()
                ' Write the data to the request stream.
                dataStream.Write(byteArray, 0, byteArray.Length)
                ' Close the Stream object.
                dataStream.Close()

                Dim MyWebResponse As HttpWebResponse = MyWebRequest.GetResponse()

                If MyWebResponse.StatusCode = "200" Then

                    'Now, we read the response (the string), and output it.
                    Dim MyResponse As Stream = MyWebResponse.GetResponseStream()
                    Dim MyResponseReader As StreamReader = New StreamReader(MyResponse)

                    'Read the response Stream and write it
                    Return MyResponseReader.ReadToEnd()

                    MyResponseReader.Close()
                    MyWebResponse.Close()
                Else
                    Return MyWebResponse.StatusCode
                End If

            Catch ex As Exception
                Return "Error: " & ex.ToString
            End Try
        Else
            Return "0"
        End If

    End Function

    'Description: To log every request and response from OTT to OMG system.
    Function OMGAPILog(ByVal MyRequest As INT_APILog) As Integer

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            Dim cmd As New SqlCommand("OMGAPILog", strConn)

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@TMAccountID", SqlDbType.VarChar, 50).Value = MyRequest.TM_AccountID

            If Trim(MyRequest.OMGProductID) <> "" Then
                cmd.Parameters.Add("@OMGProductID", SqlDbType.Int).Value = MyRequest.OMGProductID
            End If

            cmd.Parameters.Add("@EventName", SqlDbType.VarChar, 30).Value = MyRequest.EventName

            If Trim(MyRequest.DataReq) <> "" Then
                cmd.Parameters.Add("@DataReq", SqlDbType.VarChar, -1).Value = MyRequest.DataReq
            End If
            If Trim(MyRequest.DataResp) <> "" Then
                cmd.Parameters.Add("@DataResp", SqlDbType.VarChar, -1).Value = MyRequest.DataResp
            End If

            If Trim(MyRequest.ProcessStatus) <> "" Then
                cmd.Parameters.Add("@ProcessStatus", SqlDbType.VarChar, -1).Value = MyRequest.ProcessStatus
            End If

            cmd.ExecuteNonQuery()

        Catch ex As Exception
            'Do nothing
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return 0
    End Function

    Function GetAccount(ByVal TM_AccountID As String, Optional ByVal AccountType As String = "", Optional ByVal ClientType As String = "") As AccountDetails

        Dim MyResponse As New AccountDetails

        'HsiID      - For communication between end user
        'LoginID    - For communication in OMG
        'ServiceID  - For communication between NOVA/ICP
        'SubscriberID - For communication between IPTV platform

        Dim strAccount() As String
        Dim SubscriberID As String = ""

        MyResponse.HsiID = TM_AccountID

        If Trim(ClientType) <> "" And ClientType = "STB" Then

            strAccount = TM_AccountID.Split("@")
            Dim accountLength = strAccount.Length

            If AccountType = "Unifi" Then
                MyResponse.LoginID = strAccount(0) & "@unifi"
                MyResponse.ServiceID = strAccount(0) & "@unifi"
                MyResponse.SubscriberID = TM_AccountID
                MyResponse.AccountType = "Unifi"
            Else
                MyResponse.LoginID = strAccount(0) & "@streamyx"
                MyResponse.ServiceID = strAccount(0)
                MyResponse.SubscriberID = TM_AccountID
                MyResponse.AccountType = "Streamyx"
            End If
            MyResponse.ClientType = "STB"
        Else
            If TM_AccountID.Contains("@") = True Then

                strAccount = TM_AccountID.Split("@")

                If (strAccount(1) = "unifi") Or (strAccount(1) = "hsbb") Then

                    MyResponse.LoginID = TM_AccountID
                    MyResponse.ServiceID = TM_AccountID

                    If Trim(AccountType) <> "" Then

                        If Trim(AccountType) = "Unifi" Then
                            MyResponse.SubscriberID = strAccount(0) & "@iptv"
                        Else
                            MyResponse.SubscriberID = strAccount(0) & "@tvos"
                        End If
                        MyResponse.AccountType = AccountType
                    Else
                        MyResponse.SubscriberID = strAccount(0) & "@iptv"
                        MyResponse.AccountType = "Unifi"
                    End If

                ElseIf strAccount(1) = "iptv" Then
                    MyResponse.LoginID = strAccount(0) & "@unifi"
                    MyResponse.ServiceID = strAccount(0) & "@unifi"
                    MyResponse.SubscriberID = TM_AccountID
                    MyResponse.AccountType = "Unifi"
                    MyResponse.ClientType = "STB"

                ElseIf strAccount(1) = "streamyx" Then
                    MyResponse.LoginID = TM_AccountID
                    MyResponse.ServiceID = strAccount(0)
                    MyResponse.SubscriberID = strAccount(0) & "@tvos"
                    MyResponse.AccountType = "Streamyx"

                ElseIf strAccount(1) = "tvos" Then
                    MyResponse.LoginID = strAccount(0) & "@streamyx"
                    MyResponse.ServiceID = strAccount(0)
                    MyResponse.SubscriberID = TM_AccountID
                    MyResponse.AccountType = "Streamyx"
                    MyResponse.ClientType = "STB"

                ElseIf (strAccount(1) = "unifibiz") And ((TM_AccountID = "vas_newmedia@unifibiz") Or (TM_AccountID = "hyppcarnival1@unifibiz")) Then

                    MyResponse.LoginID = TM_AccountID
                    MyResponse.ServiceID = TM_AccountID
                    MyResponse.SubscriberID = strAccount(0) & "@iptv"
                    MyResponse.AccountType = "Unifi"
                Else
                    MyResponse.ServiceID = "-"
                End If
            Else
                MyResponse.ServiceID = TM_AccountID
                MyResponse.LoginID = TM_AccountID & "@streamyx"
                MyResponse.SubscriberID = TM_AccountID & "@tvos"
                MyResponse.AccountType = "Streamyx"
            End If
            MyResponse.ClientType = "WEB"

        End If

        Return MyResponse
    End Function

    Function GetMerchantDetails(ByVal OTT_ID As String, ByVal MerchantID As String) As MerchantDetails

        Dim MyDetails As New MerchantDetails

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            'Create reader
            Dim oRSItem As SqlDataReader
            Dim cmd As New SqlCommand("GetMerchantDetails", strConn)
            cmd.CommandType = CommandType.StoredProcedure

            If Trim(OTT_ID) <> "" Then
                cmd.Parameters.Add("@ott_id", SqlDbType.VarChar, 30).Value = OTT_ID     'merchantName
            ElseIf Trim(MerchantID) <> "" Then
                cmd.Parameters.Add("@merchantID", SqlDbType.VarChar, 10).Value = MerchantID
            End If

            oRSItem = cmd.ExecuteReader

            If oRSItem.HasRows Then
                oRSItem.Read()
                MyDetails.MerchantID = oRSItem("merchantID")
                MyDetails.MerchantName = oRSItem("merchantName")
                MyDetails.MerchantPWD = oRSItem("merchantPwd")
                MyDetails.TokenValidity = oRSItem("token_validity")
                MyDetails.CodeValidity = oRSItem("code_validity")
                MyDetails.Status = oRSItem("statusdesc")
                MyDetails.MerchantType = oRSItem("merchant_type")
                MyDetails.SubscriptionOrder = oRSItem("subscription_order")
                MyDetails.TerminationOrder = oRSItem("termination_order")
                MyDetails.ResponseCode = 0
                MyDetails.ResponseMsg = "Successful Transaction"
            Else
                MyDetails.ResponseCode = 1
                MyDetails.ResponseMsg = "OTT ID not match"
            End If

            oRSItem.Close()

        Catch ex As Exception
            MyDetails.ResponseCode = 2
            MyDetails.ResponseMsg = ex.ToString
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return MyDetails
    End Function

    Function GetOMGProductDetails(ByVal MerchantID As String, ByVal ProductID As String) As OMGProductDetails

        Dim MyProductDetails As New OMGProductDetails

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            'Create reader
            Dim oRSItem As SqlDataReader
            Dim cmd As New SqlCommand("GetOMGProductDetails", strConn)
            cmd.CommandType = CommandType.StoredProcedure

            If Trim(MerchantID) <> "" Then
                cmd.Parameters.Add("@merchantID", SqlDbType.VarChar, 10).Value = MerchantID
            End If

            cmd.Parameters.Add("@productID", SqlDbType.VarChar, 50).Value = ProductID
            oRSItem = cmd.ExecuteReader

            If oRSItem.HasRows Then
                oRSItem.Read()
                MyProductDetails.OMGProductID = oRSItem("omg_id")
                MyProductDetails.OMGProductCode = oRSItem("omg_productcode")
                MyProductDetails.OMGProductName = oRSItem("productName")
                MyProductDetails.OMGProductType = oRSItem("typeDesc")
                MyProductDetails.Status = oRSItem("statusdesc")

                MyProductDetails.ResponseCode = 0
                MyProductDetails.ResponseMsg = "Successful Transaction"
            Else
                MyProductDetails.ResponseCode = 1
                MyProductDetails.ResponseMsg = "Product ID not exist"
            End If

        Catch ex As Exception
            MyProductDetails.ResponseCode = 2
            MyProductDetails.ResponseMsg = ex.ToString
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return MyProductDetails
    End Function

    Function PackValidation(ByVal OMGProductID As Integer, ByVal SubscriberID As String, ByVal MerchantID As String) As PostResponse

        Dim MyResponse As New PostResponse

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            Dim cmd As New SqlCommand("GetPackValidation", strConn)

            Dim Status As New SqlParameter("@Status", SqlDbType.VarChar, 10)
            Status.Direction = ParameterDirection.Output

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@OMGProductID", SqlDbType.Int).Value = OMGProductID
            cmd.Parameters.Add("@SubscriberID", SqlDbType.VarChar, 50).Value = SubscriberID
            cmd.Parameters.Add("@MerchantID", SqlDbType.VarChar, 10).Value = MerchantID
            cmd.Parameters.Add(Status)
            cmd.ExecuteNonQuery()

            MyResponse.Status = Status.Value
            MyResponse.ResponseCode = 0
            MyResponse.ResponseMsg = "Successful Transaction"

        Catch ex As Exception
            MyResponse.ResponseCode = 2
            MyResponse.ResponseMsg = ex.ToString
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return MyResponse
    End Function

    Function ProductValidation(ByVal TM_AccountID As String, ByVal OMGProductID As Integer, Optional ByVal OTT_UserID As String = "", Optional ByVal SelfChecking As Boolean = False) As PostResponse

        Dim MyResponse As New PostResponse

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            Dim cmd As New SqlCommand("GetProductValidation", strConn)

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@TMAccountID", SqlDbType.VarChar, 50).Value = TM_AccountID
            cmd.Parameters.Add("@OMGProductID", SqlDbType.Int).Value = OMGProductID

            If Trim(OTT_UserID) <> "" Then
                cmd.Parameters.Add("@OTTUserID", SqlDbType.VarChar, 50).Value = OTT_UserID
            End If

            If SelfChecking = True Then
                cmd.Parameters.Add("@SelfChecking", SqlDbType.Int, 1).Value = 1
            End If

            cmd.Parameters.Add("@Status", SqlDbType.VarChar, 10)
            cmd.Parameters("@Status").Direction = ParameterDirection.Output
            cmd.Parameters.Add("@UserID", SqlDbType.VarChar, 50)
            cmd.Parameters("@UserID").Direction = ParameterDirection.Output
            cmd.Parameters.Add("@ActivationDate", SqlDbType.DateTime)
            cmd.Parameters("@ActivationDate").Direction = ParameterDirection.Output
            cmd.Parameters.Add("@SubscribeTo", SqlDbType.VarChar, 50)
            cmd.Parameters("@SubscribeTo").Direction = ParameterDirection.Output

            cmd.ExecuteNonQuery()

            MyResponse.Status = cmd.Parameters("@Status").Value.ToString()

            If MyResponse.Status = "3" Then
                If Not cmd.Parameters("@SubscribeTo").Value Is System.DBNull.Value Then
                    MyResponse.OMGProductCode = cmd.Parameters("@SubscribeTo").Value.ToString()
                End If

            ElseIf MyResponse.Status = "4" Then
                MyResponse.UserID = cmd.Parameters("@UserID").Value.ToString()
            ElseIf MyResponse.Status = "5" Then
                MyResponse.OMGProductCode = cmd.Parameters("@SubscribeTo").Value.ToString()
            End If

            If Not cmd.Parameters("@ActivationDate").Value Is System.DBNull.Value Then
                MyResponse.ActivationDate = cmd.Parameters("@ActivationDate").Value
            End If

            MyResponse.ResponseCode = 0
            MyResponse.ResponseMsg = "Successful Transaction"

        Catch ex As Exception
            MyResponse.ResponseCode = 2
            MyResponse.ResponseMsg = ex.ToString
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return MyResponse
    End Function

    Function ProductSubscription(ByVal MyRequest As ProductSubsReq) As PostResponse

        Dim MyResponse As New PostResponse

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            Dim cmd As New SqlCommand("ProductSubscription", strConn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@ServiceID", SqlDbType.VarChar, 50).Value = MyRequest.ServiceID
            cmd.Parameters.Add("@LoginID", SqlDbType.VarChar, 50).Value = MyRequest.LoginID

            If Trim(MyRequest.OTT_UserID) <> "" Then
                cmd.Parameters.Add("@OTTUserID", SqlDbType.VarChar, 50).Value = MyRequest.OTT_UserID
            End If

            cmd.Parameters.Add("@MerchantID", SqlDbType.VarChar, 10).Value = MyRequest.OTTMerchantID
            cmd.Parameters.Add("@OMGProductID", SqlDbType.Int).Value = MyRequest.OMGProductID

            If (Trim(MyRequest.TMBillID.ToString) <> "") And (Trim(MyRequest.TMProductID.ToString) <> "") Then
                cmd.Parameters.Add("@TMBillID", SqlDbType.Int).Value = CInt(MyRequest.TMBillID)
                cmd.Parameters.Add("@TMProductID", SqlDbType.Int).Value = CInt(MyRequest.TMProductID)
            End If

            If Trim(MyRequest.TMMerchantID) <> "" Then
                cmd.Parameters.Add("@TMMerchantID", SqlDbType.VarChar, 10).Value = MyRequest.TMMerchantID
            End If

            If Trim(MyRequest.OMG_TXNID) <> "" Then
                cmd.Parameters.Add("@OMGTXNID", SqlDbType.VarChar, 15).Value = MyRequest.OMG_TXNID
            End If

            cmd.Parameters.Add("@OrderMode", SqlDbType.Int).Value = MyRequest.OrderMode

            cmd.Parameters.Add("@Status", SqlDbType.VarChar, 10)
            cmd.Parameters("@Status").Direction = ParameterDirection.Output
            cmd.Parameters.Add("@ActivationDate", SqlDbType.DateTime)
            cmd.Parameters("@ActivationDate").Direction = ParameterDirection.Output

            cmd.ExecuteNonQuery()

            Dim Status As String = cmd.Parameters("@Status").Value.ToString()

            If Status = 1 Then
                MyResponse.ActivationDate = cmd.Parameters("@ActivationDate").Value
            End If
            MyResponse.Status = Status
            MyResponse.ResponseCode = 0
            MyResponse.ResponseMsg = "Successful Transaction"

        Catch ex As Exception
            MyResponse.ResponseCode = 2
            MyResponse.ResponseMsg = ex.ToString
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return MyResponse
    End Function

    Function SetToken(ByVal MyRequest As OMG_TokenReq, ByVal LoginID As String, ByVal Key As String, ByVal Token As String, Optional ByVal OTC_ID As Integer = 0) As PostResponse

        Dim MyResponse As New PostResponse

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            Dim cmd As New SqlCommand("InsertToken", strConn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.Add("@OTT_ID", SqlDbType.VarChar, 50).Value = MyRequest.OTT_ID
            cmd.Parameters.Add("@OTT_TXNID", SqlDbType.VarChar, 15).Value = MyRequest.OTT_TXNID
            cmd.Parameters.Add("@TMAccountID", SqlDbType.VarChar, 50).Value = LoginID
            cmd.Parameters.Add("@ProductID", SqlDbType.VarChar, 50).Value = MyRequest.ProductID
            cmd.Parameters.Add("@KeyToken", SqlDbType.VarChar, 50).Value = Key
            cmd.Parameters.Add("@Token", SqlDbType.VarChar, -1).Value = Token
            cmd.Parameters.Add("@OTCID", SqlDbType.Int).Value = OTC_ID

            If Trim(MyRequest.CallBackURL) <> "" Then
                cmd.Parameters.Add("@CallBackURL", SqlDbType.VarChar, -1).Value = MyRequest.CallBackURL
            End If

            cmd.Parameters.Add("@Status", SqlDbType.VarChar, 10)
            cmd.Parameters("@Status").Direction = ParameterDirection.Output

            cmd.ExecuteNonQuery()

            MyResponse.Status = cmd.Parameters("@Status").Value.ToString()

            MyResponse.ResponseCode = 0
            MyResponse.ResponseMsg = "Successful Transaction"

        Catch ex As Exception
            MyResponse.ResponseCode = 2
            MyResponse.ResponseMsg = ex.ToString
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return MyResponse
    End Function

    Function GetToken(ByVal Token As String) As INT_VerifyToken

        Dim MyResponse As New INT_VerifyToken

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            If Trim(Token) <> "" Then

                'Create reader
                Dim oRSItem As SqlDataReader
                Dim cmd As New SqlCommand("VerifyToken", strConn)
                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.Add("@Token", SqlDbType.VarChar, -1).Value = Token
                oRSItem = cmd.ExecuteReader

                If oRSItem.HasRows Then
                    oRSItem.Read()
                    MyResponse.OTT_ID = oRSItem("OTT_ID")
                    MyResponse.OTT_TXNID = oRSItem("OTT_TXNID")
                    MyResponse.TM_AccountID = oRSItem("TM_AccountID")
                    MyResponse.ProductType = oRSItem("typeDesc")
                    MyResponse.OTC_ID = oRSItem("otc_id")

                    If String.IsNullOrEmpty(oRSItem("CallBackURL").ToString().Trim) Then
                        MyResponse.CallBackURL = ""
                    Else
                        MyResponse.CallBackURL = oRSItem("CallBackURL")
                    End If

                    MyResponse.ResponseCode = 0
                    MyResponse.ResponseMsg = "Successful Transaction"
                Else
                    MyResponse.ResponseCode = 1
                    MyResponse.ResponseMsg = "Token not match"
                End If

                oRSItem.Close()
            Else
                MyResponse.ResponseCode = 2
                MyResponse.ResponseMsg = "Token is null"
            End If

        Catch ex As Exception
            MyResponse.ResponseCode = 2
            MyResponse.ResponseMsg = ex.ToString
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return MyResponse
    End Function

    Function SetVCode(ByVal MyRequest As INT_SetVCodeReq) As PostResponse

        Dim MyResponse As New PostResponse
        Dim IVCode As String = GenerateRandomString(4, True)

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            Dim cmd As New SqlCommand("InsertVCode", strConn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.Add("@OTT_ID", SqlDbType.VarChar, 50).Value = MyRequest.OTT_ID
            cmd.Parameters.Add("@OTT_TXNID", SqlDbType.VarChar, 15).Value = MyRequest.OTT_TXNID
            cmd.Parameters.Add("@TMAccountID", SqlDbType.VarChar, 50).Value = MyRequest.TM_AccountID
            cmd.Parameters.Add("@ProductID", SqlDbType.VarChar, 50).Value = MyRequest.ProductID
            cmd.Parameters.Add("@InitialVCode", SqlDbType.VarChar, 4).Value = IVCode
            cmd.Parameters.Add("@MobileNumber", SqlDbType.VarChar, 20).Value = MyRequest.MobileNumber
            cmd.Parameters.Add("@CustomerEmail", SqlDbType.VarChar, 50).Value = MyRequest.CustomerEmail
            cmd.Parameters.Add("@Id", SqlDbType.VarChar, 50).Value = MyRequest.Id
            cmd.Parameters.Add("@IdType", SqlDbType.VarChar, 50).Value = MyRequest.IdType
            cmd.Parameters.Add("@CycleDate", SqlDbType.VarChar, 10).Value = MyRequest.TM_BillCycle
            cmd.Parameters.Add("@VCodeValidity", SqlDbType.Int).Value = MyRequest.VCodeValidity

            cmd.Parameters.Add("@Status", SqlDbType.VarChar, 10)
            cmd.Parameters("@Status").Direction = ParameterDirection.Output
            cmd.Parameters.Add("@VCode", SqlDbType.Int)
            cmd.Parameters("@VCode").Direction = ParameterDirection.Output
            cmd.Parameters.Add("@ValidityEndDate", SqlDbType.DateTime)
            cmd.Parameters("@ValidityEndDate").Direction = ParameterDirection.Output

            cmd.ExecuteNonQuery()

            MyResponse.Status = cmd.Parameters("@Status").Value.ToString()
            MyResponse.InitialVCode = IVCode
            MyResponse.VCode = cmd.Parameters("@VCode").Value.ToString()

            If MyRequest.VCodeValidity <> 0 Then
                MyResponse.ValidityEndDate = cmd.Parameters("@ValidityEndDate").Value
            End If

            MyResponse.ResponseCode = 0
            MyResponse.ResponseMsg = "Successful Transaction"

        Catch ex As Exception
            MyResponse.ResponseCode = 2
            MyResponse.ResponseMsg = ex.ToString
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return MyResponse
    End Function

    Function GetVCode(ByVal VCode As String) As INT_GetVCodeResp

        Dim MyResponse As New INT_GetVCodeResp

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            'Create reader
            Dim oRSItem As SqlDataReader
            Dim cmd As New SqlCommand("VerifyVCode", strConn)
            cmd.CommandType = CommandType.StoredProcedure

            If Trim(VCode) <> "" Then
                Dim result() As String
                result = VCode.Split("-")
                cmd.Parameters.Add("@InitialVCode", SqlDbType.VarChar, 4).Value = result(0)
                cmd.Parameters.Add("@VCode", SqlDbType.VarChar, 6).Value = result(1)
            End If

            oRSItem = cmd.ExecuteReader()

            'Return value:
            ' 0: FAILED -- code not exist in system
            ' 1: SUCCESS
            ' 2: INVALID
            ' 3: EXPIRED

            If oRSItem.HasRows Then
                oRSItem.Read()

                MyResponse.OTT_ID = oRSItem("OTT_ID")
                MyResponse.OTT_TXNID = oRSItem("OTT_TXNID")
                MyResponse.OMG_TXNID = oRSItem("OMG_TXNID")
                MyResponse.TM_AccountID = oRSItem("TM_AccountID")
                MyResponse.ProductID = oRSItem("ProductID")
                MyResponse.MobileNumber = oRSItem("MobileNumber")
                MyResponse.CustomerEmail = oRSItem("CustomerEmail")
                MyResponse.TM_BillCycle = oRSItem("TM_BillCycle")
                MyResponse.Status = oRSItem("Status")

                If oRSItem("ReturnValue") = "1" Then
                    MyResponse.ResponseCode = 1
                    MyResponse.ResponseMsg = "Successful Transaction"

                ElseIf oRSItem("ReturnValue") = "3" Then
                    MyResponse.ResponseCode = 3
                    MyResponse.ResponseMsg = "Verification code expired"
                Else
                    MyResponse.ResponseCode = 2
                    MyResponse.ResponseMsg = "Invalid Verification code"
                End If
            Else
                MyResponse.ResponseCode = 0
                MyResponse.ResponseMsg = "Failed"
            End If

            oRSItem.Close()

        Catch ex As Exception
            MyResponse.ResponseCode = -2
            MyResponse.ResponseMsg = ex.ToString
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return MyResponse
    End Function

    'String date format: mm/dd/yyyy
    Function CalculateServiceDay(ByVal ProductType As String, ByVal TMProductName As String, ByVal ActivationDate As String, ByVal CycleDate As Integer) As Integer

        Dim ServiceDay As Integer = 0
        Dim EndDate As Date

        If ProductType = "OTC" Then

            '1.1. Set date to be compared
            EndDate = DateTime.ParseExact("09/30/2016", "MM/dd/yyyy", Nothing)
            '1.2. Calculate service day
            ServiceDay = DateDiff(DateInterval.Day, DateTime.ParseExact(ActivationDate, "MM/dd/yyyy", Nothing), EndDate)

        Else 'ALA-CARTE

            '1.1. Get current date = termination date.
            Dim TempDate As DateTime = DateTime.Now
            Dim Format As String = "MM/dd/yyyy"
            Dim TerminationDate As String = TempDate.ToString(Format)
            Dim strTerminationDate() As String = TerminationDate.Split("/")
            Dim strActivateDate() As String = ActivationDate.Split("/")

            '1.2. Set end date to be compared.
            'EndDate = DateTime.ParseExact(strTerminationDate(0) & "/" & strActivateDate(1) & "/" & strTerminationDate(2), "MM/dd/yyyy", Nothing)

            Dim IntMonth As Integer = Integer.Parse(strTerminationDate(0))
            Dim IntDate As Integer = Integer.Parse(strActivateDate(1))
            Dim IntYear As Integer = Integer.Parse(strTerminationDate(2))
            Dim EndDateString As String = ""

            Dim MonthlyDay() As Integer

            If DateTime.IsLeapYear(IntYear) Then
                MonthlyDay = {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
            Else
                MonthlyDay = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
            End If

            If IntDate > MonthlyDay(IntMonth) Then

                Dim AddDate As Integer = IntDate - MonthlyDay(IntMonth)

                IntDate = AddDate
                IntMonth += 1

                If IntMonth > 12 Then
                    IntMonth = 1
                    IntYear += 1
                End If
            End If

            Dim SMonth As String = Convert.ToString(IntMonth)
            Dim SDate As String = Convert.ToString(IntDate)

            If IntMonth < 10 Then
                SMonth = "0" & SMonth
            End If

            If IntDate < 10 Then
                SDate = "0" & SDate
            End If

            EndDateString = SMonth & "/" & SDate & "/" & IntYear
            EndDate = DateTime.ParseExact(EndDateString, "MM/dd/yyyy", Nothing)

            '1.3. Calculate total month
            Dim TotalMonth As Long
            TotalMonth = DateDiff(DateInterval.Month, DateTime.ParseExact(ActivationDate, "MM/dd/yyyy", Nothing), EndDate)

            If ProductType = "ALA-CARTE-ANNUALLY" Then

                If TotalMonth < 12 Then     'Less than a year
                    ServiceDay = 364

                Else        'More than a year

                    'A2. Calculate year
                    Dim TotalYear As Integer = CInt(TotalMonth / 12)
                    'A3. Calculate balance of the month
                    Dim BalanceMonth As Integer = TotalMonth Mod 12

                    ServiceDay = TotalYear * 365

                    If BalanceMonth = 0 Then    'Termination month = Activation month

                        If Integer.Parse(strTerminationDate(1)) > Integer.Parse(strActivateDate(1)) And
                            Integer.Parse(strTerminationDate(1)) < CycleDate Then       'User get extra time

                            'A4. Set new activation date for comparison, set activation year to termination year
                            Dim CompareDate As Date
                            CompareDate = DateTime.ParseExact(strActivateDate(0) & "/" & strActivateDate(1) & "/" & strTerminationDate(2), "MM/dd/yyyy", Nothing)

                            ServiceDay = ServiceDay + CInt(DateDiff(DateInterval.Day, DateTime.ParseExact(CompareDate, "MM/dd/yyyy", Nothing), EndDate)) - 1

                        ElseIf Integer.Parse(strTerminationDate(1)) > CycleDate Then
                            ServiceDay = (ServiceDay + 365) - 1
                        End If
                    Else
                        ServiceDay = (ServiceDay + 365) - 1
                    End If
                End If

            ElseIf ProductType = "ALA-CARTE-MONTHLY" Then

                'M3. Calculate service day, formula: (TotalMonth * 30 days) - 1 day
                ServiceDay = (TotalMonth * 30) - 1

                If Integer.Parse(strActivateDate(1)) < CycleDate Then
                    ServiceDay = ServiceDay + 30
                End If

                If Integer.Parse(strTerminationDate(1)) >= CycleDate Then
                    ServiceDay = ServiceDay + 30
                End If

            End If
        End If

        Return ServiceDay
    End Function

    Function PackTermination(ByVal SubscriberID As String, ByVal IPTVProductID As String) As PostResponse

        'TERMINATE ONLY

        Dim MyResponse As New PostResponse

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            Dim cmd As New SqlCommand("PackTermination", strConn)

            Dim Status As New SqlParameter("@Status", SqlDbType.VarChar, 10)
            Status.Direction = ParameterDirection.Output

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@SubscriberID", SqlDbType.VarChar, 50).Value = SubscriberID
            cmd.Parameters.Add("@IPTVProductID", SqlDbType.VarChar, 50).Value = IPTVProductID
            cmd.Parameters.Add(Status)
            cmd.ExecuteNonQuery()

            MyResponse.Status = Status.Value
            MyResponse.ResponseCode = 0
            MyResponse.ResponseMsg = "Successful Transaction"

        Catch ex As Exception
            MyResponse.ResponseCode = 2
            MyResponse.ResponseMsg = ex.ToString
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return MyResponse
    End Function

    Function ProductTermination(ByVal OMGProductID As Integer, ByVal TM_AccountID As String, ByVal TerminateStatus As String, ByVal TerminateBy As String, Optional ByVal ServiceDay As Integer = 0) As PostResponse

        'INACTIVE / PENDING_TERMINATION ONLY

        Dim MyResponse As New PostResponse

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            Dim cmd As New SqlCommand("ProductTermination", strConn)

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@OMGProductID", SqlDbType.Int).Value = OMGProductID
            cmd.Parameters.Add("@TMAccountID", SqlDbType.VarChar, 50).Value = TM_AccountID
            cmd.Parameters.Add("@TerminateStatus", SqlDbType.VarChar, 50).Value = TerminateStatus
            cmd.Parameters.Add("@TerminateBy", SqlDbType.VarChar, 50).Value = TerminateBy
            cmd.Parameters.Add("@ServiceDay", SqlDbType.Int).Value = ServiceDay

            cmd.Parameters.Add("@Status", SqlDbType.VarChar, 10)
            cmd.Parameters("@Status").Direction = ParameterDirection.Output
            cmd.Parameters.Add("@TerminationDate", SqlDbType.VarChar, 50)
            cmd.Parameters("@TerminationDate").Direction = ParameterDirection.Output
            cmd.Parameters.Add("@ServiceEndDate", SqlDbType.VarChar, 50)
            cmd.Parameters("@ServiceEndDate").Direction = ParameterDirection.Output

            cmd.ExecuteNonQuery()

            MyResponse.Status = cmd.Parameters("@Status").Value.ToString()
            If MyResponse.Status = 1 Then
                MyResponse.TerminationDate = cmd.Parameters("@TerminationDate").Value
                MyResponse.ServiceEndDate = cmd.Parameters("@ServiceEndDate").Value
            End If
            MyResponse.ResponseCode = 0
            MyResponse.ResponseMsg = "Successful Transaction"

        Catch ex As Exception
            MyResponse.ResponseCode = 2
            MyResponse.ResponseMsg = ex.ToString
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return MyResponse
    End Function

    Function TerminationToOTT(ByVal MerchantName As String, ByVal MerchantPWD As String, ByVal TM_AccountID As String, ByVal OMGProductCode As String, ByVal OMGProductID As String) As PostResponse

        '- Send termination to OTT -> then trigger ProductTermination
        '- If return = Success, SET TerminateStatus = INACTIVE, else SET TerminateStatus = PENDING_TERMINATION 

        Dim MyResponse As New PostResponse

        Dim b4hashstring As String
        Dim MySignature As String
        Dim DataToPost As String
        Dim URL As String = ""
        Dim MyResponseString As String

        Dim CDRToOTTReq As New OTT_TerminateReq
        Dim CDRToOTTResp As New OTT_TerminateResp

        b4hashstring = "&&" + MerchantName + "##" + MerchantPWD + "##" + TM_AccountID + "&&"
        MySignature = GetEncryption(b4hashstring)

        CDRToOTTReq.OTT_ID = MerchantName
        CDRToOTTReq.TerminationType = 1
        CDRToOTTReq.TM_AccountID = TM_AccountID
        CDRToOTTReq.ProductID = OMGProductCode
        CDRToOTTReq.Signature = MySignature

        If MerchantName = "vuclip" Then
            URL = "https://prod-in.viu.com/api/users/v2/deactivate"
        Else

        End If

        If URL IsNot Nothing Then
            'Send termination to OTT via HTTP post
            DataToPost = GetSerializeXML(CDRToOTTReq, CDRToOTTReq.GetType)
            DataToPost = Replace(DataToPost, "<?xml version=""1.0"" encoding=""utf-8""?>", "")
            MyResponseString = HTTPPost(DataToPost, URL)
            CDRToOTTResp = GetDeserializeXML(MyResponseString, CDRToOTTResp.GetType)

            CDRToOTTResp.ResponseCode = "0"     'PLEASE COMMENT WHEN DOING TESTING TO PARTNER

            If CDRToOTTResp.ResponseCode = "0" Then
                MyResponse.TerminateStatus = "inactive"
            Else
                MyResponse.TerminateStatus = "pending_termination"
            End If

            MyResponse.ResponseCode = 0
            MyResponse.ResponseMsg = "Successful Transaction"
        Else
            'Insert into ErrorTransaction_tbl
            MyResponse.ResponseCode = 101
            MyResponse.ResponseMsg = "Technical error - URL NULL"
        End If

        Return MyResponse
    End Function

    Function WriteToLog(ByVal LogReq As FileLogReq) As Integer

        Dim FilePath As String = String.Format(LogReq.FolderDirectory & "{0}", LogReq.FileLogName)

        'Get a StreamWriter class that can be used to write to the file
        Dim objStreamWriter As StreamWriter
        objStreamWriter = File.AppendText(FilePath)

        'Append the the end of the string, "A user viewed this demo at: "
        'followed by the current date and time
        objStreamWriter.WriteLine("Time : " & DateTime.Now.ToString())
        objStreamWriter.WriteLine("Status : " & LogReq.ProcessStatus)

        'Close the stream
        objStreamWriter.Close()

    End Function

End Class




