﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography
Imports System.Net
Imports System.Xml
Imports System.Xml.Serialization
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System

Public Class File_Function

    Dim ftpReq As FtpWebRequest = Nothing
    Dim ftpResp As FtpWebResponse = Nothing
    Dim ftpReader As StreamReader = Nothing
    Dim ftpStream As Stream = Nothing

    Dim FtpFile As String = ""
    Dim LocalFile As String = ""

    Function FtpFileList(ByVal URL As String, ByVal user As String, ByVal pwd As String) As List(Of String)

        Dim lines As New List(Of String)

        Try
            ftpReq = CType(FtpWebRequest.Create(URL), FtpWebRequest)
            ftpReq.Credentials = New NetworkCredential(user, pwd)
            ftpReq.Method = WebRequestMethods.Ftp.ListDirectory
            ftpResp = CType(ftpReq.GetResponse(), FtpWebResponse)
            ftpReader = New StreamReader(ftpResp.GetResponseStream())

            Dim line = ftpReader.ReadLine()

            Do Until line Is Nothing
                lines.Add(line)
                line = ftpReader.ReadLine()
            Loop

        Catch ex As Exception
            'Do nothing
        Finally
            ftpReq = Nothing
            ftpResp.Close()

            If ftpReader IsNot Nothing Then
                ftpReader.Close()
            End If
        End Try

        Return lines
    End Function

    Function DownloadFtpFile(ByVal URL As String, ByVal user As String, ByVal pwd As String, ByVal Directory As String, ByVal FileName As String) As PostResponse

        Dim MyResponse As New PostResponse

        Try
            FtpFile = URL & FileName
            LocalFile = Directory & FileName

            ftpReq = CType(FtpWebRequest.Create(FtpFile), FtpWebRequest)
            ftpReq.Credentials = New NetworkCredential(user, pwd)
            ftpReq.KeepAlive = False
            ftpReq.Proxy = Nothing
            ftpReq.UsePassive = False
            ftpReq.Method = WebRequestMethods.Ftp.DownloadFile

            ftpResp = CType(ftpReq.GetResponse(), FtpWebResponse)
            ftpReader = New StreamReader(ftpResp.GetResponseStream())

            If File.Exists(LocalFile) Then
                File.Delete(LocalFile)
            End If

            If ftpResp.ContentLength > 0 Then
                If (Not File.Exists(LocalFile)) Then
                    Dim file As New System.IO.StreamWriter(LocalFile)
                    file.WriteLine(ftpReader.ReadToEnd)
                    file.Close()
                End If

                MyResponse.ResponseCode = 0
                MyResponse.ResponseMsg = "Download completed : " & ftpResp.StatusCode
            Else
                MyResponse.ResponseCode = 101
                MyResponse.ResponseMsg = "NO data in file : " & ftpResp.StatusCode
            End If

            ftpResp.Close()

        Catch ex As Exception
            If ftpReader IsNot Nothing Then
                ftpReader.Close()
                ftpReader.Dispose()
            End If
            MyResponse.ResponseCode = 101
            MyResponse.ResponseMsg = ex.ToString
        Finally
            ftpReq = Nothing
            ftpResp.Close()

            If ftpReader IsNot Nothing Then
                ftpReader.Close()
            End If
        End Try

        Return MyResponse
    End Function

    Function DeleteFtpFiles(ByVal URL As String, ByVal user As String, ByVal pwd As String, ByVal Directory As String, ByVal FileName As String) As PostResponse

        Dim MyResponse As New PostResponse

        Try
            FtpFile = URL & FileName
            LocalFile = Directory & FileName

            If File.Exists(LocalFile) Then

                ftpReq = CType(FtpWebRequest.Create(FtpFile), FtpWebRequest)
                ftpReq.Credentials = New NetworkCredential(user, pwd)
                ftpReq.KeepAlive = False
                ftpReq.Proxy = Nothing
                ftpReq.UsePassive = False
                ftpReq.Method = WebRequestMethods.Ftp.DeleteFile

                ftpResp = ftpReq.GetResponse()

                If ftpResp.StatusCode = 250 Then
                    MyResponse.ResponseCode = 0
                    MyResponse.ResponseMsg = "Deletion completed"
                Else
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = ftpResp.StatusCode & " -" & ftpResp.StatusDescription
                End If
            Else
                MyResponse.ResponseCode = 101
                MyResponse.ResponseMsg = "Cannot delete, local file NOT exist"
            End If

        Catch ex As Exception
            MyResponse.ResponseCode = 101
            MyResponse.ResponseMsg = ex.ToString
        Finally
            ftpReq = Nothing
            ftpResp.Close()
        End Try

        Return MyResponse
    End Function

    Function UploadFtpFiles(ByVal URL As String, ByVal user As String, ByVal pwd As String, ByVal Directory As String, ByVal FileName As String) As PostResponse

        Dim MyResponse As New PostResponse

        Try
            FtpFile = URL & FileName
            LocalFile = Directory & FileName

            If File.Exists(LocalFile) Then

                ftpReq = CType(WebRequest.Create(FtpFile), FtpWebRequest)
                ftpReq.Credentials = New NetworkCredential(user, pwd)
                ftpReq.Method = WebRequestMethods.Ftp.UploadFile

                Dim sourceStream As StreamReader = New StreamReader(LocalFile)
                Dim fileContents As Byte() = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd())
                sourceStream.Close()
                ftpReq.ContentLength = fileContents.Length
                Dim requestStream As Stream = ftpReq.GetRequestStream()
                requestStream.Write(fileContents, 0, fileContents.Length)
                requestStream.Close()

                ftpResp = CType(ftpReq.GetResponse(), FtpWebResponse)

                If ftpResp.StatusCode = 226 Then

                    MyResponse.ResponseCode = 0
                    MyResponse.ResponseMsg = "Upload completed"
                Else
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = ftpResp.StatusCode & " -" & ftpResp.StatusDescription
                End If

            Else
                MyResponse.ResponseCode = 101
                MyResponse.ResponseMsg = "Cannot upload, local file NOT exist"
            End If

        Catch ex As Exception
            MyResponse.ResponseCode = 101
            MyResponse.ResponseMsg = ex.ToString
        Finally
            ftpReq = Nothing
            ftpResp.Close()
        End Try

        Return MyResponse

    End Function

End Class
