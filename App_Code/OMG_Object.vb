﻿Imports Microsoft.VisualBasic
Imports System.Xml.Serialization

Public Class OMG_Object

End Class

Public Class PostResponse
    Public UserID As String
    Public OMGProductID As Integer
    Public OMGProductCode As String
    Public MerchantName As String
    Public MerchantPWD As String
    Public TerminateStatus As String
    Public TerminationDate As DateTime
    Public ActivationDate As DateTime
    Public ServiceEndDate As String
    Public TM_BillCycle As String
    Public OSMActivationDate As String
    Public TMBillID As Integer
    Public Status As String
    Public ErrorMsg As String
    Public MobileNumber As String

    'For Verification Code - Added by Haidah 23 July 2017
    Public InitialVCode As String
    Public VCode As String
    Public ValidityEndDate As DateTime
    '---------------------------------------------

    Public ResponseCode As String
    Public ResponseMsg As String
End Class

Public Class MerchantDetails
    Public MerchantID As String
    Public MerchantName As String
    Public MerchantPWD As String
    Public TokenValidity As Integer
    Public CodeValidity As Integer
    Public Status As String
    Public MerchantType As String   'Client, Billing, Internal
    Public SubscriptionOrder As String
    Public TerminationOrder As String
    Public ResponseCode As String
    Public ResponseMsg As String
End Class

Public Class AccountDetails
    Public HsiID As String          'For communication between end user
    Public LoginID As String        'For communication in OMG
    Public ServiceID As String      'For communication between NOVA/ICP (@unifi / @streamyx)
    Public SubscriberID As String   'For communication between IPTV platform (@iptv / @tvos)
    Public AccountType As String    'Options: Unifi or Streamyx
    Public ClientType As String     'Options: STB or WEB
End Class

Public Class OMGProductDetails
    Public OMGProductID As Integer
    Public OMGProductCode As String
    Public OMGProductName As String
    Public OMGProductType As String
    Public Status As String
    Public ResponseCode As String
    Public ResponseMsg As String
End Class

<XmlRoot("OMG_ValidateReq")>
Public Class OMG_ValidateReq
    Public OTT_ID As String
    Public TM_AccountID As String
    Public ProductID As String
    Public OTT_UserID As String
    Public Signature As String
End Class

<XmlRoot("OMG_ValidateResp")>
Public Class OMG_ValidateResp
    Public OTT_ID As String
    Public TM_AccountID As String
    Public Status As String
    Public OTT_UserID As String
    Public ResponseCode As String
    Public ResponseMsg As String
End Class

Public Class ProductSubsReq
    Public ServiceID As String
    Public LoginID As String
    Public OTT_UserID As String
    Public OTTMerchantID As String
    Public OMGProductID As Integer
    Public TMBillID As String
    Public TMProductID As String
    Public TMMerchantID As String
    Public OMG_TXNID As String
    Public OrderMode As Integer
End Class

<XmlRoot("OMG_ValidationReq")>
Public Class OMG_ValidationReq
    Public OTT_ID As String
    Public TM_AccountID As String
    Public ProductID As String
    Public Signature As String
End Class

<XmlRoot("OMG_ValidationResp")>
Public Class OMG_ValidationResp
    Public OTT_ID As String
    Public TM_AccountID As String
    Public Status As String
    Public ProductID As String
    Public ResponseCode As String
    Public ResponseMsg As String
End Class

<XmlRoot("OMG_TokenReq")>
Public Class OMG_TokenReq
    Public OTT_ID As String
    Public OTT_TXNID As String
    Public TM_AccountID As String
    Public ProductID As String
    Public CallBackURL As String
    Public Signature As String
End Class

<XmlRoot("OMG_TokenResp")>
Public Class OMG_TokenResp
    Public OTT_ID As String
    Public OTT_TXNID As String
    Public TM_AccountID As String
    Public Token As String
    Public ResponseCode As String
    Public ResponseMsg As String
End Class

Public Class INT_VerifyToken
    Public OTT_ID As String
    Public OTT_TXNID As String
    Public TM_AccountID As String
    'Public Token As String
    Public ProductType As String
    Public OTC_ID As Integer
    Public CallBackURL As String
    Public ResponseCode As String
    Public ResponseMsg As String
End Class

<XmlRoot("OMG_VCodeReq")>
Public Class OMG_VCodeReq
    Public OTT_ID As String
    Public OTT_TXNID As String
    Public TM_AccountID As String
    Public ProductID As String
    Public Signature As String
End Class

<XmlRoot("OMG_VCodeResp")>
Public Class OMG_VCodeResp
    Public OTT_ID As String
    Public OTT_TXNID As String
    Public TM_AccountID As String
    Public InitialVCode As String
    Public MobileNumber As String
    Public CustomerEmail As String
    Public ResponseCode As String
    Public ResponseMsg As String
End Class

Public Class INT_SetVCodeReq
    Public OTT_ID As String
    Public OTT_TXNID As String
    Public TM_AccountID As String
    Public ProductID As String
    Public MobileNumber As String
    Public CustomerEmail As String
    Public Id As String
    Public IdType As String
    Public TM_BillCycle As String
    Public VCodeValidity As String
End Class

Public Class INT_GetVCodeResp
    Public OTT_ID As String
    Public OTT_TXNID As String
    Public OMG_TXNID As String
    Public TM_AccountID As String
    Public ProductID As String
    Public MobileNumber As String
    Public CustomerEmail As String
    Public TM_BillCycle As String
    Public Status As String
    Public ResponseCode As String
    Public ResponseMsg As String
End Class

<XmlRoot("OMG_NewOrderReq")>
Public Class OMG_NewOrderReq
    Public OTT_ID As String
    Public OrderMode As Integer
    Public OMG_TXNID As String
    Public OTT_TXNID As String
    Public TM_AccountID As String
    Public ProductID As String
    Public VerificationCode As String
    Public Signature As String
End Class

<XmlRoot("OMG_NewOrderResp")>
Public Class OMG_NewOrderResp
    Public OTT_ID As String
    Public OMG_TXNID As String
    Public TM_AccountID As String
    Public ProductID As String
    Public ActivationDate As String
    Public ResponseCode As String
    Public ResponseMsg As String
End Class

<XmlRoot("OTT_TerminateReq")>
Public Class OTT_TerminateReq
    Public OTT_ID As String
    Public TerminationType As Integer
    Public TM_AccountID As String
    Public ProductID As String
    Public Signature As String
End Class

<XmlRoot("OTT_TerminateResp")>
Public Class OTT_TerminateResp
    Public OTT_ID As String
    Public TM_AccountID As String
    Public ProductID As String
    Public OTT_TerminationDate As String
    Public OTT_ServiceEndDate As String
    Public ResponseCode As String
    Public ResponseMsg As String
End Class

'Public Class GeneralResponse
'    Public ResponseCode As String
'    Public ResponseMsg As String
'End Class

Public Class FileLogReq
    Public FolderDirectory As String
    Public FileLogName As String
    Public ProcessStatus As String
End Class