﻿Imports Microsoft.VisualBasic

Public Class Mail_Function

    Function SendViaEmail(ByVal MyRequest As INT_SendMailReq) As INT_SendMailResp

        Dim MyResponse As New INT_SendMailResp

        'Create instance of main mail message class.
        Dim mailMessage As System.Net.Mail.MailMessage = New System.Net.Mail.MailMessage()

        'Set To addresses
        mailMessage.To.Add(New System.Net.Mail.MailAddress(MyRequest.Email))

        mailMessage.IsBodyHtml = True
        mailMessage.Subject = MyRequest.Subject     'message text format. Can be text or html
        mailMessage.Body = MyRequest.MsgBody        'message body


        mailMessage.Priority = System.Net.Mail.MailPriority.Normal      'email priority. Can be low, normal or high

        'Create an instance of the SmtpClient class for sending the email
        Dim SmtpMail As New System.Net.Mail.SmtpClient()

        Try
            'mail server used to send this email. modify this line based on your mail server
            SmtpMail.Send(mailMessage)          'using the static method "Send" of the SmtpMail class to send the mail

            MyResponse.ResponseCode = 0
            MyResponse.ResponseMsg = "OK"

        Catch smtpExc As System.Net.Mail.SmtpException

            MyResponse.ResponseCode = smtpExc.StatusCode
            MyResponse.ResponseMsg = smtpExc.Message

        Catch ex As Exception
            MyResponse.ResponseCode = 1
            MyResponse.ResponseMsg = ex.Message
        End Try

        Return MyResponse
    End Function


End Class
