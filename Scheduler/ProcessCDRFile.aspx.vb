﻿Imports System
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web.Services.Description
Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography
Imports System.Net
Imports System.Xml.Serialization
Imports System.Xml
Imports System.Xml.Schema
Imports System.Globalization

Partial Class ProcessCDRFile
    Inherits System.Web.UI.Page

    Dim MyOMGFunction As New OMG_Function
    Dim FTPFunction As New File_Function
    Dim MyDb As New dbConn
    Dim strConn As New SqlConnection(MyDb.connString)

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        'Dim MyResponse As New OMG_ValidateResp
        'MyResponse = MyFunction.ProcessCDR()

        'Dim ResponseString As String = OMGFunction.GetSerializeXML(MyResponse, MyResponse.GetType)
        'LiteralRequest.Text = Server.HtmlEncode("-")
        'LiteralResponse.Text = Server.HtmlEncode(ResponseString)

        Dim theDay = DateTime.Now.ToString("dd")
        Dim theMonth = DateTime.Now.ToString("MMMM").ToUpper()
        Dim theYear = DateTime.Now.Year

        Dim RawFileDir As String = "E:\www\OMG_prod\CDR\RAW\"
        Dim NewFileDir As String = "E:\www\OMG_prod\CDR\NEW\"
        Dim ProcessFileDir As String = "E:\www\OMG_prod\CDR\PROCESS\" & theMonth & "_" & theYear & "\"
        Dim ErrorFileDir As String = "E:\www\OMG_prod\CDR\ERROR\"

        If Directory.Exists(ProcessFileDir) Then
            'Do nothing
        Else
            Directory.CreateDirectory(ProcessFileDir)
        End If

        Dim WriteLogReq As New FileLogReq
        WriteLogReq.FolderDirectory = ProcessFileDir
        WriteLogReq.FileLogName = theDay & "_" & theMonth & "_" & theYear & ".txt"

        'Step 1: Read all file in /RAW directory
        Dim fileEntries As String() = Directory.GetFiles(RawFileDir)

        If fileEntries.Length > 0 Then
            Dim fullpath As String = ""
            Dim fileName As String = ""

            For Each fullpath In fileEntries
                fileName = fullpath.Replace(RawFileDir, "")

                If Path.GetExtension(fileName) = ".dat" Then
                    'Step 2: Read all lines in the file and remove last line containing "Record Total".
                    Dim lines As New List(Of String)(IO.File.ReadAllLines(fullpath))

                    If lines.Count > 0 Then
                        Dim RecordTotal As Integer = lines.Count - 1
                        If lines(RecordTotal).Contains("Record Total") Then
                            lines.RemoveAt(RecordTotal)
                        End If

                        'Step 3: Create and save new data to NEW file with the same filename in /NEW directory.
                        Dim NewFileName As String = fullpath.Replace(RawFileDir, NewFileDir)
                        IO.File.WriteAllLines(NewFileName, lines)

                        'Step 4: FTP NEW file to database server
                        Dim FtpResponse As New PostResponse
                        FtpResponse = FTPFunction.UploadFtpFiles("ftp://10.45.96.49/OMGArchieve/", "ftpuser49", "typ3ftp%49", NewFileDir, fileName)

                        If FtpResponse.ResponseCode = "0" Then
                            'Step 4: Bulk insert

                            Try
                                If strConn.State = ConnectionState.Closed Then
                                    strConn.Open()
                                End If

                                Dim cmd As New SqlCommand("ProcessCDRBulk", strConn)
                                cmd.CommandType = CommandType.StoredProcedure
                                cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 100).Value = fileName

                                cmd.Parameters.Add("@Status", SqlDbType.VarChar, 20)
                                cmd.Parameters("@Status").Direction = ParameterDirection.Output

                                cmd.ExecuteNonQuery()

                                Dim Status As String = cmd.Parameters("@Status").Value.ToString()
                                'Step 5: Delete file in database server (FTP),
                                '        If "SUCCESS", move file from RAW to PROCESS.
                                '        If "FAILED", move file from RAW to ERROR.
                                '       Both need to delete file in NEW directory & FTP folder.

                                Dim FileRawPath As String = fullpath

                                If Status = "SUCCESS" Then
                                    Dim FileProcessPath As String = fullpath.Replace(RawFileDir, ProcessFileDir)
                                    System.IO.File.Move(FileRawPath, FileProcessPath)
                                Else
                                    Dim FileErrorPath As String = fullpath.Replace(RawFileDir, ErrorFileDir)
                                    System.IO.File.Move(FileRawPath, FileErrorPath)

                                    WriteLogReq.ProcessStatus = "CDR Bulk failed; Filename: " & fileName
                                    MyOMGFunction.WriteToLog(WriteLogReq)
                                End If

                                FtpResponse = FTPFunction.DeleteFtpFiles("ftp://10.45.96.49/OMGArchieve/", "ftpuser49", "typ3ftp%49", NewFileDir, fileName)
                                If FtpResponse.ResponseCode = "0" Then
                                    'Do nothing
                                Else
                                    WriteLogReq.ProcessStatus = "CDR Bulk delete FTP file failed: " & FtpResponse.ResponseMsg & "; Filename: " & fileName
                                    MyOMGFunction.WriteToLog(WriteLogReq)
                                End If
                                System.IO.File.Delete(NewFileName)

                            Catch ex As Exception
                                'Do nothing
                            Finally
                                If strConn.State = ConnectionState.Open Then
                                    strConn.Close()
                                End If
                            End Try
                        Else
                            WriteLogReq.ProcessStatus = "CDR Bulk FTP failed: " & FtpResponse.ResponseMsg & "; Filename: " & fileName
                            MyOMGFunction.WriteToLog(WriteLogReq)
                        End If
                    End If
                End If

            Next fullpath
        End If

    End Sub
End Class
