﻿<%@ WebService Language="VB" Class="OMGSystem" %>

Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web.Services.Description
Imports System.Web.Script.Services
Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography
Imports System.Net
Imports System.Xml.Serialization
Imports System.Xml
Imports System.Xml.Schema

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://omg.hypp.tv/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
Public Class OMGSystem
    Inherits System.Web.Services.WebService

    Dim MyOMGFunction As New OMG_Function
    Dim MyTMFunction As New TMBilling_Function
    Dim MyMerchant As New MerchantDetails
    Dim b4hashstring As String
    Dim MySignature As String
    Dim APILog As New INT_APILog
    Dim ErrorMessage As String = ""

    <WebMethod(Description:="To validate TM account ID based on HyppTV pack subscription.")>
    Public Function OMG_PackValidation(ByVal MyRequest As OMG_ValidateReq) As OMG_ValidateResp

        Dim MyResponse As New OMG_ValidateResp
        Dim MyOMGProduct As New OMGProductDetails
        Dim CheckPack As Boolean = False
        Dim ProductActivation As Boolean = False

        MyMerchant = MyOMGFunction.GetMerchantDetails(MyRequest.OTT_ID, "")

        '1. Check merchant exist & active
        If MyMerchant.ResponseCode = 0 And UCase(MyMerchant.Status) = "ACTIVE" Then

            '2. Check request contain TM_AccountID & ProductID
            If (Trim(MyRequest.TM_AccountID) <> "") And (Trim(MyRequest.ProductID) <> "") Then

                b4hashstring = "&&" + MyRequest.OTT_ID + "##" + MyMerchant.MerchantPWD + "##" + MyRequest.TM_AccountID + "&&"
                MySignature = MyOMGFunction.GetEncryption(b4hashstring)

                '3. Check matching signature
                If MySignature = MyRequest.Signature Then

                    '4. Check product exist & active
                    MyOMGProduct = MyOMGFunction.GetOMGProductDetails(MyMerchant.MerchantID, MyRequest.ProductID)

                    If MyOMGProduct.ResponseCode = 0 And UCase(MyOMGProduct.Status) = "ACTIVE" Then

                        '5. Check TM_AccountID
                        Dim GetAccountResp As New AccountDetails
                        GetAccountResp = MyOMGFunction.GetAccount(MyRequest.TM_AccountID)

                        If GetAccountResp.ServiceID <> "-" Then

                            '6. Product validation
                            Dim ValidateProduct As New PostResponse
                            ValidateProduct = MyOMGFunction.ProductValidation(GetAccountResp.LoginID, MyOMGProduct.OMGProductID, MyRequest.OTT_UserID, True)

                            If ValidateProduct.ResponseCode = 0 Then

                                Select Case ValidateProduct.Status
                                    Case 1, 3, 4
                                        MyResponse.OTT_ID = MyRequest.OTT_ID
                                        MyResponse.TM_AccountID = GetAccountResp.LoginID
                                        MyResponse.Status = ValidateProduct.Status

                                        If ValidateProduct.Status = 4 Then
                                            MyResponse.OTT_UserID = ValidateProduct.UserID
                                        End If
                                        MyResponse.ResponseCode = 0
                                        MyResponse.ResponseMsg = "Successful validation"
                                    Case Else   'Status = 0
                                        CheckPack = True
                                End Select
                            Else
                                MyResponse.ResponseCode = 101
                                MyResponse.ResponseMsg = "Technical error"
                                ErrorMessage = "ProductValidation: " & ValidateProduct.ResponseMsg & "(" & ValidateProduct.ResponseCode & ")"
                            End If

                            '7. Pack validation
                            If CheckPack = True Then

                                Dim ValidatePack As New PostResponse
                                ValidatePack = MyOMGFunction.PackValidation(MyOMGProduct.OMGProductID, GetAccountResp.SubscriberID, MyMerchant.MerchantID)

                                If ValidatePack.ResponseCode = 0 Then

                                    Select Case ValidatePack.Status
                                        Case 1
                                            ProductActivation = True
                                        Case 2
                                            MyResponse.OTT_ID = MyRequest.OTT_ID
                                            MyResponse.TM_AccountID = GetAccountResp.LoginID
                                            MyResponse.Status = 2
                                            MyResponse.ResponseCode = 0
                                            MyResponse.ResponseMsg = "Successful validation"
                                        Case -1
                                            MyResponse.ResponseCode = 103
                                            MyResponse.ResponseMsg = "NO product bundling"
                                        Case Else
                                            MyResponse.ResponseCode = 101
                                            MyResponse.ResponseMsg = "Technical error"
                                            ErrorMessage = "PackValidation status: " & ValidatePack.Status
                                    End Select
                                Else
                                    MyResponse.ResponseCode = 101
                                    MyResponse.ResponseMsg = "Technical error"
                                    ErrorMessage = "PackValidation: " & ValidatePack.ResponseMsg & "(" & ValidatePack.ResponseCode & ")"
                                End If
                            End If

                            '8. Product subscription
                            If ProductActivation = True Then

                                If (Trim(MyRequest.OTT_UserID) <> "") Then

                                    Dim ActivateProdReq As New ProductSubsReq
                                    Dim ActivateProdResp As New PostResponse

                                    ActivateProdReq.ServiceID = GetAccountResp.ServiceID
                                    ActivateProdReq.LoginID = GetAccountResp.LoginID
                                    ActivateProdReq.OTT_UserID = MyRequest.OTT_UserID
                                    ActivateProdReq.OTTMerchantID = MyMerchant.MerchantID
                                    ActivateProdReq.OMGProductID = MyOMGProduct.OMGProductID
                                    ActivateProdReq.TMBillID = ""
                                    ActivateProdReq.TMProductID = ""
                                    ActivateProdReq.TMMerchantID = ""

                                    ActivateProdResp = MyOMGFunction.ProductSubscription(ActivateProdReq)

                                    If ActivateProdResp.ResponseCode = 0 Then

                                        Select Case ActivateProdResp.Status
                                            Case 0
                                                MyResponse.ResponseCode = 106
                                                MyResponse.ResponseMsg = "Failed transaction"
                                            Case 1
                                                MyResponse.OTT_ID = MyRequest.OTT_ID
                                                MyResponse.TM_AccountID = GetAccountResp.LoginID
                                                MyResponse.Status = 1
                                                MyResponse.ResponseCode = 0
                                                MyResponse.ResponseMsg = "Successful activation"
                                            Case Else
                                                MyResponse.ResponseCode = 106
                                                MyResponse.ResponseMsg = "Failed transaction"
                                                ErrorMessage = "ProductSubscription Status: " & ActivateProdResp.Status
                                                'perlu
                                        End Select
                                    Else
                                        MyResponse.ResponseCode = 101
                                        MyResponse.ResponseMsg = "Technical error"
                                        ErrorMessage = "ProductSubscription: " & ActivateProdResp.ResponseMsg & "(" & ActivateProdResp.ResponseCode & ")"
                                    End If
                                Else
                                    MyResponse.OTT_ID = MyRequest.OTT_ID
                                    MyResponse.TM_AccountID = GetAccountResp.LoginID
                                    MyResponse.Status = 1
                                    MyResponse.ResponseCode = 0
                                    MyResponse.ResponseMsg = "Successful validation"
                                End If
                            End If
                        Else
                            MyResponse.ResponseCode = 104
                            MyResponse.ResponseMsg = "Invalid TM_AccountID"
                        End If
                    Else
                        Select Case MyOMGProduct.ResponseCode
                            Case 0, 1       'INACTIVE & NOT EXIST
                                MyResponse.ResponseCode = 103
                                MyResponse.ResponseMsg = "Product ID not exist"
                            Case 2
                                MyResponse.ResponseCode = 101
                                MyResponse.ResponseMsg = "Technical error"
                                ErrorMessage = "OMG: " & MyOMGProduct.ResponseMsg
                            Case Else
                                MyResponse.ResponseCode = 101
                                MyResponse.ResponseMsg = "Technical error"
                                ErrorMessage = "OMG: " & MyOMGProduct.ResponseMsg & "(" & MyOMGProduct.ResponseCode & ")"
                        End Select
                    End If
                Else
                    MyResponse.ResponseCode = "100"
                    MyResponse.ResponseMsg = "Signature does not match"
                End If
            Else
                MyResponse.ResponseCode = 105
                MyResponse.ResponseMsg = "Missing parameter"
            End If
        Else
            Select Case MyMerchant.ResponseCode
                Case 0, 1       'INACTIVE & NOT EXIST
                    MyResponse.ResponseCode = 102
                    MyResponse.ResponseMsg = "Invalid OTT ID"
                Case 2
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg
                Case Else
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error - " & MyMerchant.ResponseCode
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg & "(" & MyMerchant.ResponseCode & ")"
            End Select
        End If

        Dim DataToPost As String = MyOMGFunction.GetSerializeXML(MyRequest, MyRequest.GetType)
        Dim MyResponseString As String = MyOMGFunction.GetSerializeXML(MyResponse, MyResponse.GetType)

        APILog.TM_AccountID = MyRequest.TM_AccountID
        APILog.OMGProductID = MyOMGProduct.OMGProductID
        APILog.EventName = "OMG_PackValidation"
        APILog.DataReq = DataToPost
        APILog.DataResp = MyResponseString

        If MyResponse.ResponseCode = "101" Or
            (MyResponse.ResponseCode <> "0" And ErrorMessage <> "") Then
            APILog.ProcessStatus = ErrorMessage

        ElseIf MyResponse.ResponseCode <> "0" Then
            APILog.ProcessStatus = MyResponse.ResponseCode & " - " & MyResponse.ResponseMsg
        End If
        MyOMGFunction.OMGAPILog(APILog)

        Return MyResponse
    End Function

    <WebMethod(Description:="To validate product subscriptions for TM account ID.")>
    Public Function OMG_ProductValidation(ByVal MyRequest As OMG_ValidationReq) As OMG_ValidationResp

        Dim MyResponse As New OMG_ValidationResp
        Dim MyOMGProduct As New OMGProductDetails

        MyMerchant = MyOMGFunction.GetMerchantDetails(MyRequest.OTT_ID, "")

        '1. Check merchant exist & active
        If MyMerchant.ResponseCode = 0 And UCase(MyMerchant.Status) = "ACTIVE" Then

            '2. Check request contain TM_AccountID & ProductID
            If (Trim(MyRequest.TM_AccountID) <> "") And (Trim(MyRequest.ProductID) <> "") Then

                b4hashstring = "&&" + MyRequest.OTT_ID + "##" + MyMerchant.MerchantPWD + "##" + MyRequest.TM_AccountID + "&&"
                MySignature = MyOMGFunction.GetEncryption(b4hashstring)

                '3. Check matching signature
                If MySignature = MyRequest.Signature Then

                    '4. Check TM_AccountID
                    Dim GetAccountResp As New AccountDetails
                    GetAccountResp = MyOMGFunction.GetAccount(MyRequest.TM_AccountID)

                    If GetAccountResp.ServiceID <> "-" Then

                        '5. Check product EXIST & ACTIVE
                        MyOMGProduct = MyOMGFunction.GetOMGProductDetails(MyMerchant.MerchantID, MyRequest.ProductID)

                        If MyOMGProduct.ResponseCode = 0 And UCase(MyOMGProduct.Status) = "ACTIVE" Then

                            '6. Check if user has subscribe to the product
                            '- check in ProductSubscription_tbl based on omg_id (ProductValidation)                                     
                            Dim ValidateProduct As New PostResponse
                            ValidateProduct = MyOMGFunction.ProductValidation(GetAccountResp.LoginID, MyOMGProduct.OMGProductID)

                            If ValidateProduct.ResponseCode = 0 Then

                                MyResponse.OTT_ID = MyRequest.OTT_ID
                                MyResponse.TM_AccountID = GetAccountResp.LoginID

                                MyResponse.ResponseCode = 0
                                MyResponse.ResponseMsg = "Successful validation"

                                Select Case ValidateProduct.Status
                                    Case 0
                                        MyResponse.Status = 2
                                    Case 3
                                        MyResponse.Status = 1
                                        If Trim(ValidateProduct.OMGProductCode) <> "" Then
                                            MyResponse.ProductID = ValidateProduct.OMGProductCode
                                        Else
                                            MyResponse.ProductID = MyRequest.ProductID
                                        End If
                                        MyResponse.ResponseCode = 106
                                        MyResponse.ResponseMsg = "Pending termination"
                                    Case 5
                                        MyResponse.Status = 3
                                        MyResponse.ProductID = ValidateProduct.OMGProductCode
                                    Case 1, 4   'Status = 1, 4
                                        MyResponse.Status = 1
                                        MyResponse.ProductID = MyRequest.ProductID
                                    Case Else
                                        MyResponse.ResponseCode = 106
                                        MyResponse.ResponseMsg = "Failed transaction"
                                End Select
                            Else
                                MyResponse.ResponseCode = 101
                                MyResponse.ResponseMsg = "Technical error"
                                ErrorMessage = "ProductValidation: " & ValidateProduct.ResponseMsg & "(" & ValidateProduct.ResponseCode & ")"
                            End If
                        Else
                            Select Case MyOMGProduct.ResponseCode
                                Case 0, 1       'INACTIVE & NOT EXIST
                                    MyResponse.ResponseCode = 103
                                    MyResponse.ResponseMsg = "Product ID not exist"
                                Case 2
                                    MyResponse.ResponseCode = 101
                                    MyResponse.ResponseMsg = "Technical error"
                                    ErrorMessage = "OMG: " & MyOMGProduct.ResponseMsg
                                Case Else
                                    MyResponse.ResponseCode = 101
                                    MyResponse.ResponseMsg = "Technical error"
                                    ErrorMessage = "OMG: " & MyOMGProduct.ResponseMsg & "(" & MyOMGProduct.ResponseCode & ")"
                            End Select
                        End If
                    Else
                        MyResponse.ResponseCode = 104
                        MyResponse.ResponseMsg = "Invalid TM_AccountID"
                    End If
                Else
                    MyResponse.ResponseCode = "100"
                    MyResponse.ResponseMsg = "Signature does not match"
                End If
            Else
                MyResponse.ResponseCode = 105
                MyResponse.ResponseMsg = "Missing parameter"
            End If
        Else
            Select Case MyMerchant.ResponseCode
                Case 0, 1       'INACTIVE & NOT EXIST
                    MyResponse.ResponseCode = 102
                    MyResponse.ResponseMsg = "Invalid OTT ID"
                Case 2
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg
                Case Else
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg & "(" & MyMerchant.ResponseCode & ")"
            End Select
        End If

        Dim DataToPost As String = MyOMGFunction.GetSerializeXML(MyRequest, MyRequest.GetType)
        Dim MyResponseString As String = MyOMGFunction.GetSerializeXML(MyResponse, MyResponse.GetType)

        APILog.TM_AccountID = MyRequest.TM_AccountID
        APILog.OMGProductID = MyOMGProduct.OMGProductID
        APILog.EventName = "OMG_ProductValidation"
        APILog.DataReq = DataToPost
        APILog.DataResp = MyResponseString

        If MyResponse.ResponseCode = "101" Or
            (MyResponse.ResponseCode <> "0" And ErrorMessage <> "") Then
            APILog.ProcessStatus = ErrorMessage

        ElseIf MyResponse.ResponseCode <> "0" Then
            APILog.ProcessStatus = MyResponse.ResponseCode & " - " & MyResponse.ResponseMsg
        End If
        MyOMGFunction.OMGAPILog(APILog)

        Return MyResponse
    End Function

    <WebMethod(Description:="Request to generate token.")>
    Public Function OMG_GenerateToken(ByVal MyRequest As OMG_TokenReq) As OMG_TokenResp

        Dim MyResponse As New OMG_TokenResp
        Dim MyOMGProduct As New OMGProductDetails

        MyMerchant = MyOMGFunction.GetMerchantDetails(MyRequest.OTT_ID, "")

        '1. Check merchant exist & active
        If MyMerchant.ResponseCode = 0 And UCase(MyMerchant.Status) = "ACTIVE" Then

            '2. Check request contain TM_AccountID & ProductID
            If (Trim(MyRequest.TM_AccountID) <> "") And (Trim(MyRequest.OTT_TXNID) <> "") And (Trim(MyRequest.ProductID) <> "") Then

                b4hashstring = "&&" + MyRequest.OTT_ID + "##" + MyMerchant.MerchantPWD + "##" + MyRequest.TM_AccountID + "&&"
                MySignature = MyOMGFunction.GetEncryption(b4hashstring)

                '3. Check matching signature
                If MySignature = MyRequest.Signature Then

                    '4. Check TM_AccountID
                    Dim GetAccountResp As New AccountDetails
                    GetAccountResp = MyOMGFunction.GetAccount(MyRequest.TM_AccountID)

                    If GetAccountResp.ServiceID <> "-" Then

                        '5. Check product EXIST & ACTIVE
                        MyOMGProduct = MyOMGFunction.GetOMGProductDetails(MyMerchant.MerchantID, MyRequest.ProductID)

                        If MyOMGProduct.ResponseCode = "0" And
                                UCase(MyOMGProduct.Status) = "ACTIVE" And
                                ((MyOMGProduct.OMGProductType = "ALA-CARTE-ANNUALLY") Or (MyOMGProduct.OMGProductType = "ALA-CARTE-MONTHLY")) Then

                            '6. Check if user has subscribe to the product
                            '- check in ProductSubscription_tbl based on omg_id (ProductValidation)                                     
                            Dim ValidateProduct As New PostResponse
                            ValidateProduct = MyOMGFunction.ProductValidation(GetAccountResp.LoginID, MyOMGProduct.OMGProductID)

                            If ValidateProduct.ResponseCode = "0" Then

                                If ValidateProduct.Status = "0" Then

                                    Dim TempDate As DateTime = DateTime.Now
                                    Dim KeyToken As String = TempDate.ToString("yyyyMMddhhmmss")

                                    Dim b4hashToken As String = "&&" + MyRequest.OTT_ID + "##" + MyMerchant.MerchantPWD + "##" + KeyToken + "##" + MyRequest.TM_AccountID + "##" + MyRequest.ProductID + "&&"
                                    Dim HashToken As String = MyOMGFunction.GetEncryption(b4hashToken, True)

                                    Dim GenerateTokenResp As New PostResponse
                                    GenerateTokenResp = MyOMGFunction.SetToken(MyRequest, GetAccountResp.LoginID, KeyToken, HashToken)

                                    If GenerateTokenResp.ResponseCode = "0" And GenerateTokenResp.Status = "1" Then
                                        MyResponse.OTT_ID = MyRequest.OTT_ID
                                        If MyRequest.OTT_TXNID.Length > 15 Then
                                            MyResponse.OTT_TXNID = MyRequest.OTT_TXNID.Substring(0, 15)
                                        Else
                                            MyResponse.OTT_TXNID = MyRequest.OTT_TXNID
                                        End If
                                        MyResponse.TM_AccountID = MyRequest.TM_AccountID
                                        MyResponse.Token = HashToken
                                        MyResponse.ResponseCode = 0
                                        MyResponse.ResponseMsg = "Successful transaction"
                                    Else
                                        MyResponse.ResponseCode = 106

                                        If GenerateTokenResp.Status = "2" Then
                                            MyResponse.ResponseMsg = "Duplicate OTT_TXNID"
                                        Else
                                            MyResponse.ResponseMsg = "Failed transaction"
                                        End If
                                        ErrorMessage = "SetToken: " & GenerateTokenResp.ResponseMsg & "(" & GenerateTokenResp.ResponseCode & ")"
                                    End If

                                Else
                                    MyResponse.ResponseCode = 106
                                    Select Case ValidateProduct.Status
                                        Case "3"
                                            MyResponse.ResponseMsg = "Pending termination"
                                        Case "5"
                                            MyResponse.ResponseMsg = "Active product subscription (" & ValidateProduct.OMGProductCode & ") under the same merchant"
                                        Case Else   'Status = 1, 4
                                            MyResponse.ResponseMsg = "Already subscribe to product"
                                    End Select
                                End If

                            Else
                                MyResponse.ResponseCode = 101
                                MyResponse.ResponseMsg = "Technical error"
                                ErrorMessage = "ProductValidation: " & ValidateProduct.ResponseMsg & "(" & ValidateProduct.ResponseCode & ")"
                            End If

                        Else
                            Select Case MyOMGProduct.ResponseCode
                                Case "0", "1"       'INACTIVE & NOT EXIST
                                    MyResponse.ResponseCode = 103
                                    MyResponse.ResponseMsg = "Product ID not exist"
                                Case "2"
                                    MyResponse.ResponseCode = 101
                                    MyResponse.ResponseMsg = "Technical error"
                                    ErrorMessage = "OMG: " & MyOMGProduct.ResponseMsg
                                Case Else
                                    MyResponse.ResponseCode = 101
                                    MyResponse.ResponseMsg = "Technical error"
                                    ErrorMessage = "OMG: " & MyOMGProduct.ResponseMsg & "(" & MyOMGProduct.ResponseCode & ")"
                            End Select
                        End If
                    Else
                        MyResponse.ResponseCode = 104
                        MyResponse.ResponseMsg = "Invalid TM_AccountID"
                    End If
                Else
                    MyResponse.ResponseCode = "100"
                    MyResponse.ResponseMsg = "Signature does not match"
                End If
            Else
                MyResponse.ResponseCode = 105
                MyResponse.ResponseMsg = "Missing parameter"
            End If
        Else
            Select Case MyMerchant.ResponseCode
                Case 0, 1       'INACTIVE & NOT EXIST
                    MyResponse.ResponseCode = 102
                    MyResponse.ResponseMsg = "Invalid OTT ID"
                Case 2
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg
                Case Else
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg & "(" & MyMerchant.ResponseCode & ")"
            End Select
        End If

        Dim DataToPost As String = MyOMGFunction.GetSerializeXML(MyRequest, MyRequest.GetType)
        Dim MyResponseString As String = MyOMGFunction.GetSerializeXML(MyResponse, MyResponse.GetType)

        APILog.TM_AccountID = MyRequest.TM_AccountID
        APILog.OMGProductID = MyOMGProduct.OMGProductID
        APILog.EventName = "OMG_GenerateToken"
        APILog.DataReq = DataToPost
        APILog.DataResp = MyResponseString

        If MyResponse.ResponseCode = "101" Or
            (MyResponse.ResponseCode <> "0" And ErrorMessage <> "") Then
            APILog.ProcessStatus = ErrorMessage

        ElseIf MyResponse.ResponseCode <> "0" Then
            APILog.ProcessStatus = MyResponse.ResponseCode & " - " & MyResponse.ResponseMsg
        End If
        MyOMGFunction.OMGAPILog(APILog)

        Return MyResponse
    End Function

    <WebMethod(Description:="Request to generate verification code.")>
    Public Function OMG_GenerateVCode(ByVal MyRequest As OMG_VCodeReq) As OMG_VCodeResp

        Dim MyResponse As New OMG_VCodeResp
        Dim MyTMProduct As New TMProductDetails
        Dim ProceedSend As Boolean = False
        Dim SendByMail As Boolean = True

        MyMerchant = MyOMGFunction.GetMerchantDetails(MyRequest.OTT_ID, "")

        '1. Check merchant exist & active
        If MyMerchant.ResponseCode = "0" And UCase(MyMerchant.Status) = "ACTIVE" Then

            '2. Check request contain TM_AccountID & ProductID
            If (Trim(MyRequest.TM_AccountID) <> "") And (Trim(MyRequest.OTT_TXNID) <> "") And (Trim(MyRequest.ProductID) <> "") Then

                b4hashstring = "&&" + MyRequest.OTT_ID + "##" + MyMerchant.MerchantPWD + "##" + MyRequest.TM_AccountID + "&&"
                MySignature = MyOMGFunction.GetEncryption(b4hashstring)

                '3. Check matching signature
                If MySignature = MyRequest.Signature Then

                    '4. Check TM_AccountID
                    Dim GetAccountResp As New AccountDetails
                    GetAccountResp = MyOMGFunction.GetAccount(MyRequest.TM_AccountID)

                    If GetAccountResp.ServiceID <> "-" Then

                        '5. Check product exist & active
                        MyTMProduct = MyTMFunction.GetTMProductDetails(MyMerchant.MerchantID, MyRequest.ProductID, GetAccountResp.AccountType)

                        If MyTMProduct.ResponseCode = "0" And
                                UCase(MyTMProduct.Status) = "ACTIVE" And
                                ((MyTMProduct.ProductType = "ALA-CARTE-ANNUALLY") Or (MyTMProduct.ProductType = "ALA-CARTE-MONTHLY")) Then

                            '6. Check if user has subscribe to the product
                            '- check in ProductSubscription_tbl based on omg_id (ProductValidation)                                     
                            Dim ValidateProduct As New PostResponse
                            ValidateProduct = MyOMGFunction.ProductValidation(GetAccountResp.LoginID, MyTMProduct.OMGProductID)

                            If ValidateProduct.Status = "0" Then

                                '7. Validate TM_AccountID
                                Dim ValidateUserReq As New INT_ValidateUserReq
                                Dim ValidateUserResp As New INT_ValidateUserResp
                                ValidateUserReq.TM_AccountID = GetAccountResp.ServiceID
                                ValidateUserReq.TM_AccountType = GetAccountResp.AccountType

                                '-- Need to enable when production -----------------------------
                                ValidateUserResp = MyTMFunction.ValidateAccountId(ValidateUserReq)
                                '---------------------------------------------------------------

                                If ValidateUserResp.ResponseCode = "0" Then

                                    If UCase(ValidateUserResp.Status) = "ACTIVE" Then

                                        If (Trim(ValidateUserResp.MobileNumber) <> "") And (Not ValidateUserResp.MobileNumber Is Nothing) Then
                                            SendByMail = False
                                            ProceedSend = True
                                        ElseIf (Trim(ValidateUserResp.CustomerEmail) <> "") And (Not ValidateUserResp.CustomerEmail Is Nothing) Then
                                            SendByMail = True
                                            ProceedSend = True
                                        End If

                                        If ProceedSend = True Then

                                            '7. Generate Verification code
                                            Dim GenerateVCodeReq As New INT_SetVCodeReq
                                            Dim GenerateVCodeResp As New PostResponse

                                            GenerateVCodeReq.OTT_ID = MyRequest.OTT_ID
                                            GenerateVCodeReq.OTT_TXNID = MyRequest.OTT_TXNID
                                            GenerateVCodeReq.TM_AccountID = GetAccountResp.LoginID
                                            GenerateVCodeReq.ProductID = MyRequest.ProductID
                                            GenerateVCodeReq.MobileNumber = ValidateUserResp.MobileNumber
                                            GenerateVCodeReq.CustomerEmail = ValidateUserResp.CustomerEmail
                                            GenerateVCodeReq.Id = ValidateUserResp.Id
                                            GenerateVCodeReq.IdType = ValidateUserResp.IdType
                                            GenerateVCodeReq.TM_BillCycle = ValidateUserResp.TM_BillCycle
                                            GenerateVCodeReq.VCodeValidity = MyMerchant.CodeValidity

                                            GenerateVCodeResp = MyOMGFunction.SetVCode(GenerateVCodeReq)

                                            If GenerateVCodeResp.ResponseCode = "0" And GenerateVCodeResp.Status = "1" Then

                                                '8. Send Verification code
                                                Dim strMsg As String = ""

                                                If MyMerchant.CodeValidity <> 0 And (Not GenerateVCodeResp.ValidityEndDate.ToString Is Nothing) Then

                                                    Dim TempDate As DateTime = GenerateVCodeResp.ValidityEndDate
                                                    Dim Format As String = "ddd MMM dd HH:mm:ss yyyy"
                                                    Dim ValidityTime As String = TempDate.ToString(Format)

                                                    strMsg = "RM0.00 TM: " & MyTMProduct.TMProductName & " subscription request. One-time PIN: " & GenerateVCodeResp.InitialVCode & "-" & GenerateVCodeResp.VCode & ". Expires by " & ValidityTime & ". TQ"
                                                Else
                                                    strMsg = "RM0.00 TM: " & MyTMProduct.TMProductName & " subscription request. One-time PIN: " & GenerateVCodeResp.InitialVCode & "-" & GenerateVCodeResp.VCode & ". TQ"
                                                End If

                                                If SendByMail = True Then

                                                    Dim MyMailFunction As New Mail_Function

                                                    Dim SendMailReq As New INT_SendMailReq
                                                    Dim SendMailResp As New INT_SendMailResp

                                                    SendMailReq.Email = ValidateUserResp.CustomerEmail
                                                    SendMailReq.Subject = "unifi TV Verification Code - " & MyRequest.TM_AccountID
                                                    SendMailReq.MsgBody = strMsg.Replace("RM0.00 TM: ", "")

                                                    SendMailResp = MyMailFunction.SendViaEmail(SendMailReq)

                                                    If SendMailResp.ResponseMsg = "OK" Then

                                                        MyResponse.OTT_ID = MyRequest.OTT_ID
                                                        If MyRequest.OTT_TXNID.Length > 15 Then
                                                            MyResponse.OTT_TXNID = MyRequest.OTT_TXNID.Substring(0, 15)
                                                        Else
                                                            MyResponse.OTT_TXNID = MyRequest.OTT_TXNID
                                                        End If

                                                        MyResponse.TM_AccountID = MyRequest.TM_AccountID
                                                        MyResponse.InitialVCode = GenerateVCodeResp.InitialVCode

                                                        Dim strEmail() As String
                                                        strEmail = SendMailReq.Email.Split("@")

                                                        Dim MLength As Integer = strEmail(0).Length
                                                        Dim Asteric As String = ""

                                                        Asteric = Left(strEmail(0), 2)
                                                        For value As Integer = 0 To MLength - 4
                                                            Asteric = Asteric & "*"
                                                        Next
                                                        Asteric = Asteric & Right(strEmail(0), 1)
                                                        Dim TempEmail As String = Asteric & "@" & strEmail(1)

                                                        MyResponse.CustomerEmail = TempEmail
                                                        MyResponse.ResponseCode = 0
                                                        MyResponse.ResponseMsg = "Successful transaction"
                                                    Else
                                                        MyResponse.ResponseCode = 106
                                                        MyResponse.ResponseMsg = "Failed transaction"
                                                        ErrorMessage = "SendViaEmail: " & SendMailResp.ResponseMsg & "(" & SendMailResp.ResponseCode & ")"
                                                    End If

                                                Else
                                                    Dim IB_Func As New InfoBlast_function
                                                    Dim SendSMSReq As New INT_SendSMSReq
                                                    Dim SendSMSResp As New INT_SendSMSResp

                                                    SendSMSReq.TM_AccountID = MyRequest.TM_AccountID
                                                    SendSMSReq.MobileNumber = ValidateUserResp.MobileNumber
                                                    SendSMSReq.ProductId = MyRequest.ProductID
                                                    SendSMSReq.StrMsg = strMsg

                                                    SendSMSResp = IB_Func.SMSInfoBlast(SendSMSReq, "", GenerateVCodeResp.InitialVCode, GenerateVCodeResp.VCode)

                                                    If SendSMSResp.ResponseCode = "0" Then

                                                        MyResponse.OTT_ID = MyRequest.OTT_ID

                                                        If MyRequest.OTT_TXNID.Length > 15 Then
                                                            MyResponse.OTT_TXNID = MyRequest.OTT_TXNID.Substring(0, 15)
                                                        Else
                                                            MyResponse.OTT_TXNID = MyRequest.OTT_TXNID
                                                        End If
                                                        MyResponse.TM_AccountID = MyRequest.TM_AccountID
                                                        MyResponse.InitialVCode = GenerateVCodeResp.InitialVCode

                                                        Dim MLength As Integer = ValidateUserResp.MobileNumber.Length - 4
                                                        Dim Asteric As String = ""

                                                        For value As Integer = 0 To MLength - 1
                                                            Asteric = Asteric & "*"
                                                        Next
                                                        Dim LastFourDigits As String = ValidateUserResp.MobileNumber.Substring(MLength, 4)
                                                        Dim TempMobileNo As String = Asteric & LastFourDigits

                                                        MyResponse.MobileNumber = TempMobileNo
                                                        MyResponse.ResponseCode = 0
                                                        MyResponse.ResponseMsg = "Successful transaction"
                                                    Else
                                                        MyResponse.ResponseCode = 106
                                                        MyResponse.ResponseMsg = "Failed transaction"
                                                        ErrorMessage = "SMSInfoBlast: " & SendSMSResp.ResponseMsg & "(" & SendSMSResp.ResponseCode & ")"
                                                    End If
                                                End If

                                            Else
                                                MyResponse.ResponseCode = 106

                                                If GenerateVCodeResp.Status = "2" Then
                                                    MyResponse.ResponseMsg = "Duplicate OTT_TXNID"
                                                Else
                                                    MyResponse.ResponseMsg = "Failed transaction"
                                                    ErrorMessage = "SetVCode: " & GenerateVCodeResp.ResponseMsg & "(" & GenerateVCodeResp.ResponseCode & ")"
                                                End If
                                            End If

                                        Else
                                            MyResponse.ResponseCode = 107
                                            MyResponse.ResponseMsg = "Invalid Mobile number Or Email"
                                        End If

                                    Else
                                        MyResponse.ResponseCode = 104
                                        MyResponse.ResponseMsg = "Account is NOT active"
                                        ErrorMessage = "ValidateID Status : " & ValidateUserResp.ResponseMsg & "(" & ValidateUserResp.ResponseCode & ")"
                                    End If
                                ElseIf ValidateUserResp.ResponseCode = "103" Then
                                    MyResponse.ResponseCode = 104
                                    MyResponse.ResponseMsg = "Account is NOT active"

                                ElseIf ValidateUserResp.ResponseCode = "106" Then
                                    MyResponse.ResponseCode = 104
                                    MyResponse.ResponseMsg = "Account NOT Exist"
                                Else
                                    MyResponse.ResponseCode = 101
                                    MyResponse.ResponseMsg = "Technical error"
                                    ErrorMessage = "ValidateID: " & ValidateUserResp.ResponseMsg & "(" & ValidateUserResp.ResponseCode & ")"
                                End If

                            Else
                                MyResponse.ResponseCode = 106
                                Select Case ValidateProduct.Status
                                    Case "3"
                                        MyResponse.ResponseMsg = "Pending termination"
                                    Case "5"
                                        MyResponse.ResponseMsg = "Active product subscription (" & ValidateProduct.OMGProductCode & ") under the same merchant"
                                    Case Else   'Status = 1, 4
                                        MyResponse.ResponseMsg = "Already subscribe to product"
                                End Select
                            End If

                        Else
                            Select Case MyTMProduct.ResponseCode
                                Case "0", "1"       'INACTIVE & NOT EXIST
                                    MyResponse.ResponseCode = 103
                                    MyResponse.ResponseMsg = "Product ID not exist"
                                Case "2"
                                    MyResponse.ResponseCode = 101
                                    MyResponse.ResponseMsg = "Technical error"
                                    ErrorMessage = "TM: " & MyTMProduct.ResponseMsg
                                Case Else
                                    MyResponse.ResponseCode = 101
                                    MyResponse.ResponseMsg = "Technical error"
                                    ErrorMessage = "TM: " & MyTMProduct.ResponseMsg & "(" & MyTMProduct.ResponseCode & ")"
                            End Select
                        End If
                    Else
                        MyResponse.ResponseCode = 104
                        MyResponse.ResponseMsg = "Invalid TM_AccountID"
                    End If
                Else
                    MyResponse.ResponseCode = "100"
                    MyResponse.ResponseMsg = "Signature does not match"
                End If
            Else
                MyResponse.ResponseCode = 105
                MyResponse.ResponseMsg = "Missing parameter"
            End If

        Else
            Select Case MyMerchant.ResponseCode
                Case "0", "1"       'INACTIVE & NOT EXIST
                    MyResponse.ResponseCode = 102
                    MyResponse.ResponseMsg = "Invalid OTT ID"
                Case "2"
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg
                Case Else
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg & "(" & MyMerchant.ResponseCode & ")"
            End Select
        End If

        Dim DataToPost As String = MyOMGFunction.GetSerializeXML(MyRequest, MyRequest.GetType)
        Dim MyResponseString As String = MyOMGFunction.GetSerializeXML(MyResponse, MyResponse.GetType)

        APILog.TM_AccountID = MyRequest.TM_AccountID
        APILog.OMGProductID = MyTMProduct.OMGProductID
        APILog.EventName = "OMG_GenerateVCode"
        APILog.DataReq = DataToPost
        APILog.DataResp = MyResponseString

        If MyResponse.ResponseCode = "101" Or
            (MyResponse.ResponseCode <> "0" And ErrorMessage <> "") Then
            APILog.ProcessStatus = ErrorMessage

        ElseIf MyResponse.ResponseCode <> "0" Then
            APILog.ProcessStatus = MyResponse.ResponseCode & " - " & MyResponse.ResponseMsg
        End If
        MyOMGFunction.OMGAPILog(APILog)

        Return MyResponse
    End Function

    <WebMethod(Description:="To create order for new OTT product subscription.")>
    Public Function OMG_NewOTTOrder(ByVal MyRequest As OMG_NewOrderReq) As OMG_NewOrderResp

        Dim MyResponse As New OMG_NewOrderResp
        Dim MyTMProduct As New TMProductDetails
        Dim ProceedVerifyTranxID As Boolean = False
        Dim ProceedVerifyVCode As Boolean = False
        Dim ProceedValidation As Boolean = False
        Dim ProceedOrder As Boolean = False

        MyMerchant = MyOMGFunction.GetMerchantDetails(MyRequest.OTT_ID, "")

        '1. Check merchant exist & active
        If MyMerchant.ResponseCode = 0 And UCase(MyMerchant.Status) = "ACTIVE" Then

            '2. Check request contain TM_AccountID & ProductID
            If (Trim(MyRequest.TM_AccountID) <> "") And (Trim(MyRequest.ProductID) <> "") And
                (Trim(MyRequest.OTT_TXNID) <> "") Then

                b4hashstring = "&&" + MyRequest.OTT_ID + "##" + MyMerchant.MerchantPWD + "##" + MyRequest.TM_AccountID + "&&"
                MySignature = MyOMGFunction.GetEncryption(b4hashstring)

                '3. Check matching signature
                If MySignature = MyRequest.Signature Then

                    '4. Check TM_AccountID
                    Dim GetAccountResp As New AccountDetails
                    GetAccountResp = MyOMGFunction.GetAccount(MyRequest.TM_AccountID)

                    If GetAccountResp.ServiceID <> "-" Then

                        If MyRequest.OrderMode = 0 Then
                            MyRequest.OrderMode = 1
                        End If

                        Select Case MyRequest.OrderMode
                            Case 1, 2, 3
                                ProceedVerifyTranxID = True
                            Case 4
                                ProceedVerifyVCode = True
                            Case Else
                                ProceedValidation = False
                        End Select

                        '5. Verify OMG_TXNID or VerificationCode
                        Dim MobileNumber As String = String.Empty
                        Dim CustomerEmail As String = String.Empty
                        Dim TM_BillCycle As String = String.Empty
                        Dim OMG_TXNID As String = String.Empty

                        If ProceedVerifyTranxID = True Then

                            If Trim(MyRequest.OMG_TXNID) <> "" Then

                                Dim GetIdReq As New INT_GetTransactionIDReq
                                Dim GetIdResp As New INT_GetTransactionIDResp

                                GetIdReq.TM_AccountID = GetAccountResp.LoginID
                                GetIdReq.OMG_TXNID = MyRequest.OMG_TXNID

                                GetIdResp = MyTMFunction.GetOMGTransactionID(GetIdReq)

                                If GetIdResp.ResponseCode = "0" And GetIdResp.Status = "VALID" Then

                                    If (MyRequest.OTT_ID = GetIdResp.OTT_ID) And
                                        (MyRequest.OTT_TXNID = GetIdResp.OTT_TXNID) And
                                        (MyRequest.ProductID = GetIdResp.ProductID) Then

                                        MobileNumber = GetIdResp.MobileNumber
                                        CustomerEmail = GetIdResp.CustomerEmail
                                        TM_BillCycle = GetIdResp.TM_BillCycle
                                        OMG_TXNID = MyRequest.OMG_TXNID
                                        ProceedValidation = True
                                    Else
                                        MyResponse.ResponseCode = 108
                                        MyResponse.ResponseMsg = "Failed validation"
                                    End If
                                Else
                                    MyResponse.ResponseCode = 106
                                    MyResponse.ResponseMsg = "Invalid transaction ID"
                                End If

                            Else
                                MyResponse.ResponseCode = 106
                                MyResponse.ResponseMsg = "Invalid transaction ID"
                            End If

                        ElseIf ProceedVerifyVCode = True Then

                            If Trim(MyRequest.VerificationCode) <> "" Then

                                'Check if verification code format send is TRUE
                                Dim vcodeSend As Boolean = MyRequest.VerificationCode Like "[A-Z][A-Z][A-Z][A-Z][ -.]######"

                                If vcodeSend = True Then

                                    Dim GetVCodeResp As New INT_GetVCodeResp
                                    GetVCodeResp = MyOMGFunction.GetVCode(MyRequest.VerificationCode)

                                    If GetVCodeResp.ResponseCode = "1" And GetVCodeResp.Status = "VALID" Then

                                        If (MyRequest.OTT_ID = GetVCodeResp.OTT_ID) And
                                            (MyRequest.OTT_TXNID = GetVCodeResp.OTT_TXNID) And (GetAccountResp.LoginID = GetVCodeResp.TM_AccountID) And
                                            (MyRequest.ProductID = GetVCodeResp.ProductID) Then

                                            MobileNumber = GetVCodeResp.MobileNumber
                                            CustomerEmail = GetVCodeResp.CustomerEmail
                                            TM_BillCycle = GetVCodeResp.TM_BillCycle
                                            OMG_TXNID = GetVCodeResp.OMG_TXNID
                                            ProceedValidation = True
                                        Else
                                            MyResponse.ResponseCode = 108
                                            MyResponse.ResponseMsg = "Failed validation"
                                        End If
                                    Else
                                        If GetVCodeResp.ResponseCode = "3" Then
                                            MyResponse.ResponseCode = 109
                                            MyResponse.ResponseMsg = "Verification code expired"
                                        Else
                                            MyResponse.ResponseCode = 107
                                            MyResponse.ResponseMsg = "Invalid verification code"
                                        End If
                                        ErrorMessage = "GetVCode " & GetVCodeResp.ResponseMsg & "(" & GetVCodeResp.ResponseCode & ")"
                                    End If
                                Else
                                    MyResponse.ResponseCode = 107
                                    MyResponse.ResponseMsg = "Invalid verification code format - " & MyRequest.VerificationCode
                                End If
                            Else
                                MyResponse.ResponseCode = 107
                                MyResponse.ResponseMsg = "Invalid verification code"
                                ErrorMessage = "VerificationCode is NULL"
                            End If
                        Else
                            MyResponse.ResponseCode = 106
                            MyResponse.ResponseMsg = "Invalid transaction ID or verification code"
                        End If

                        If ProceedValidation = True Then

                            '6. Get TM product details based on ott_merchantID and omg_productcode
                            MyTMProduct = MyTMFunction.GetTMProductDetails(MyMerchant.MerchantID, MyRequest.ProductID, GetAccountResp.AccountType)

                            If MyTMProduct.ResponseCode = 0 And
                                UCase(MyTMProduct.Status) = "ACTIVE" And
                                ((MyTMProduct.ProductType = "ALA-CARTE-ANNUALLY") Or (MyTMProduct.ProductType = "ALA-CARTE-MONTHLY")) Then

                                '7. Check if user has subscribe to the product
                                '- check in ProductSubscription_tbl based on omg_id (ProductValidation)
                                '- check in BillingSubscription_tbl based on tm_id (BillingValidation)                                     
                                Dim ValidateProduct As New PostResponse
                                ValidateProduct = MyOMGFunction.ProductValidation(GetAccountResp.LoginID, MyTMProduct.OMGProductID)

                                If ValidateProduct.ResponseCode = 0 Then

                                    If ValidateProduct.Status = "0" Then

                                        Dim ValidateBill As New PostResponse
                                        ValidateBill = MyTMFunction.BillingValidation(GetAccountResp.ServiceID, MyTMProduct.TMProductID)

                                        If ValidateBill.ResponseCode = "0" Then

                                            If ValidateBill.Status = "0" Then
                                                ProceedOrder = True
                                            Else
                                                MyResponse.ResponseCode = 106
                                                Select Case ValidateBill.Status
                                                    Case 1
                                                        MyResponse.ResponseMsg = "Already subscribe to product"
                                                    Case 2
                                                        MyResponse.ResponseMsg = "Available order in process"
                                                End Select
                                            End If
                                        Else
                                            MyResponse.ResponseCode = 101
                                            MyResponse.ResponseMsg = "Technical error"
                                            ErrorMessage = "BillingValidation: " & ValidateBill.ResponseMsg & "(" & ValidateBill.ResponseCode & ")"
                                        End If
                                    Else
                                        MyResponse.ResponseCode = 106
                                        Select Case ValidateProduct.Status
                                            Case 3
                                                MyResponse.ResponseMsg = "Pending termination"
                                            Case 5
                                                MyResponse.ResponseMsg = "Active product subscription (" & ValidateProduct.OMGProductCode & ") under the same merchant"
                                            Case Else   'Status = 1, 4
                                                MyResponse.ResponseMsg = "Already subscribe to product"
                                        End Select
                                    End If
                                Else
                                    MyResponse.ResponseCode = 101
                                    MyResponse.ResponseMsg = "Technical error"
                                    ErrorMessage = "ProductValidation: " & ValidateProduct.ResponseMsg & "(" & ValidateProduct.ResponseCode & ")"
                                End If

                                '8. Process OTT order request
                                '- Send Order request to NOVA / ICP
                                '- Insert in BillingSubscription_tbl & ProductSubscription_tbl
                                If ProceedOrder = True Then
                                    Dim TempDate As DateTime = DateTime.Now
                                    Dim Format As String = "MM/dd/yyyy"
                                    Dim FormatOTT As String = "dd-MM-yyyy hh:mm:ss"
                                    Dim ActivationDate As String = TempDate.ToString(Format)
                                    Dim ActivationDateOTT As String = TempDate.ToString(FormatOTT)

                                    Dim TMOrderReq As New INT_NewOrderReq
                                    Dim TMOrderResp As New INT_NewOrderResp

                                    If MyRequest.OTT_ID = "iflix" Then
                                        TMOrderResp.ResponseCode = "0"
                                    Else
                                        '-- Need to enable when production !!!! -----------------------------
                                        TMOrderReq.ReqType = "Order"
                                        TMOrderReq.TM_AccountID = GetAccountResp.ServiceID
                                        TMOrderReq.TM_AccountType = GetAccountResp.AccountType
                                        TMOrderReq.TMProductID = MyTMProduct.TMProductID
                                        TMOrderReq.TMProductCode = MyTMProduct.TMProductCode
                                        TMOrderReq.TMProductName = MyTMProduct.TMProductName
                                        TMOrderReq.ProductType = MyTMProduct.ProductType
                                        TMOrderReq.ActivationDate = ActivationDate

                                        TMOrderResp = MyTMFunction.NewOTTOrder(TMOrderReq)
                                        '---------------------------------------------------------------  
                                    End If

                                    If TMOrderResp.ResponseCode = "0" Then

                                        Dim BillReq As New OMG_SubmitOrderRequest
                                        BillReq.TM_AccountID = GetAccountResp.ServiceID
                                        BillReq.TM_AccountType = GetAccountResp.AccountType
                                        BillReq.TM_MobileNo = MobileNumber
                                        BillReq.TM_Email = CustomerEmail

                                        Dim SubscribeBill As New PostResponse
                                        SubscribeBill = MyTMFunction.BillingSubscription(BillReq, MyTMProduct.TMProductID, MyMerchant.MerchantID, "OMG", TM_BillCycle)

                                        If SubscribeBill.ResponseCode = "0" Then

                                            If SubscribeBill.Status = "1" Then

                                                Dim SubsProductReq As New ProductSubsReq
                                                Dim SubsProductResp As New PostResponse

                                                SubsProductReq.ServiceID = GetAccountResp.ServiceID
                                                SubsProductReq.LoginID = GetAccountResp.LoginID
                                                SubsProductReq.OTT_UserID = ""
                                                SubsProductReq.OTTMerchantID = MyTMProduct.OTTMerchantID
                                                SubsProductReq.OMGProductID = MyTMProduct.OMGProductID
                                                SubsProductReq.TMBillID = SubscribeBill.TMBillID
                                                SubsProductReq.TMProductID = MyTMProduct.TMProductID
                                                SubsProductReq.OMG_TXNID = MyRequest.OMG_TXNID
                                                SubsProductReq.OrderMode = MyRequest.OrderMode

                                                SubsProductResp = MyOMGFunction.ProductSubscription(SubsProductReq)

                                                If SubsProductResp.ResponseCode = "0" Then

                                                    Select Case SubsProductResp.Status
                                                        Case 0
                                                            MyResponse.ResponseCode = 106
                                                            MyResponse.ResponseMsg = "Failed order request"
                                                        Case 1
                                                            MyResponse.OTT_ID = MyRequest.OTT_ID
                                                            MyResponse.OMG_TXNID = OMG_TXNID
                                                            MyResponse.TM_AccountID = MyRequest.TM_AccountID
                                                            MyResponse.ProductID = MyRequest.ProductID
                                                            MyResponse.ActivationDate = ActivationDateOTT

                                                            MyResponse.ResponseCode = 0
                                                            MyResponse.ResponseMsg = "Successful transaction"
                                                        Case Else
                                                            MyResponse.ResponseCode = 106
                                                            MyResponse.ResponseMsg = "Failed order request"
                                                            ErrorMessage = "ProductSubscription Status: " & SubsProductResp.Status
                                                    End Select
                                                Else
                                                    MyResponse.ResponseCode = 101
                                                    MyResponse.ResponseMsg = "Technical error"
                                                    ErrorMessage = "ProductSubscription: " & SubsProductResp.ResponseMsg & "(" & SubsProductResp.ResponseCode & ")"
                                                End If
                                            Else
                                                MyResponse.ResponseCode = 106
                                                MyResponse.ResponseMsg = "Failed order request"
                                                ErrorMessage = "BillingSubscription Status: " & SubscribeBill.Status
                                            End If

                                        Else
                                            MyResponse.ResponseCode = 101
                                            MyResponse.ResponseMsg = "Technical error"
                                            ErrorMessage = "BillingSubscription: " & SubscribeBill.ResponseMsg & "(" & SubscribeBill.ResponseCode & ")"
                                        End If
                                    Else
                                        MyResponse.ResponseCode = 106
                                        MyResponse.ResponseMsg = "Failed order creation to TM Billing"
                                    End If
                                End If

                            Else
                                Select Case MyTMProduct.ResponseCode
                                    Case 0, 1       'INACTIVE & NOT EXIST
                                        MyResponse.ResponseCode = 103
                                        MyResponse.ResponseMsg = "Product ID not exist"
                                    Case 2
                                        MyResponse.ResponseCode = 101
                                        MyResponse.ResponseMsg = "Technical error - product"
                                        ErrorMessage = "TM: " & MyTMProduct.ResponseMsg
                                    Case Else
                                        MyResponse.ResponseCode = 101
                                        MyResponse.ResponseMsg = "Technical error"
                                        ErrorMessage = "TM: " & MyTMProduct.ResponseMsg & "(" & MyTMProduct.ResponseCode & ")"
                                End Select
                            End If
                        End If
                    Else
                        MyResponse.ResponseCode = 104
                        MyResponse.ResponseMsg = "Invalid TM_AccountID"
                    End If
                Else
                    MyResponse.ResponseCode = "100"
                    MyResponse.ResponseMsg = "Signature does not match"
                End If
            Else
                MyResponse.ResponseCode = 105
                MyResponse.ResponseMsg = "Missing parameter"
            End If
        Else
            Select Case MyMerchant.ResponseCode
                Case 0, 1       'INACTIVE & NOT EXIST
                    MyResponse.ResponseCode = 102
                    MyResponse.ResponseMsg = "Invalid OTT ID"
                Case 2
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg
                Case Else
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg & "(" & MyMerchant.ResponseCode & ")"
            End Select
        End If

        Dim DataToPost As String = MyOMGFunction.GetSerializeXML(MyRequest, MyRequest.GetType)
        Dim MyResponseString As String = MyOMGFunction.GetSerializeXML(MyResponse, MyResponse.GetType)

        APILog.TM_AccountID = MyRequest.TM_AccountID
        APILog.OMGProductID = MyTMProduct.OMGProductID
        APILog.EventName = "OMG_NewOTTOrder"
        APILog.DataReq = DataToPost
        APILog.DataResp = MyResponseString

        If MyResponse.ResponseCode = "101" Or
            (MyResponse.ResponseCode <> "0" And ErrorMessage <> "") Then
            APILog.ProcessStatus = ErrorMessage

        ElseIf MyResponse.ResponseCode <> "0" Then
            APILog.ProcessStatus = MyResponse.ResponseCode & " - " & MyResponse.ResponseMsg
        End If
        MyOMGFunction.OMGAPILog(APILog)

        Return MyResponse
    End Function

    <WebMethod(Description:="To send service termination request.")>
    Public Function OTT_Termination(ByVal MyRequest As OTT_TerminateReq) As OTT_TerminateResp

        Dim MyResponse As New OTT_TerminateResp
        Dim MyTMProduct As New TMProductDetails
        Dim CalculateServiceDay As Boolean = False
        Dim ServiceDay As Integer = 0
        Dim BillCycleDate As String = ""
        Dim ProceedTermination As Boolean = False

        MyMerchant = MyOMGFunction.GetMerchantDetails(MyRequest.OTT_ID, "")

        '1. Check merchant exist & active
        If MyMerchant.ResponseCode = 0 And UCase(MyMerchant.Status) = "ACTIVE" Then

            '2. Check request contain TM_AccountID & ProductID
            If (Trim(MyRequest.TM_AccountID) <> "") And (Trim(MyRequest.ProductID) <> "") Then

                b4hashstring = "&&" + MyRequest.OTT_ID + "##" + MyMerchant.MerchantPWD + "##" + MyRequest.TM_AccountID + "&&"
                MySignature = MyOMGFunction.GetEncryption(b4hashstring)

                '3. Check matching signature
                If MySignature = MyRequest.Signature Then

                    '4. Check TM_AccountID
                    Dim GetAccountResp As New AccountDetails
                    GetAccountResp = MyOMGFunction.GetAccount(MyRequest.TM_AccountID)

                    If GetAccountResp.ServiceID <> "-" Then

                        '5. Get TM product details based on ott_merchantID and omg_productcode
                        MyTMProduct = MyTMFunction.GetTMProductDetails(MyMerchant.MerchantID, MyRequest.ProductID, GetAccountResp.AccountType)

                        If MyTMProduct.ResponseCode = 0 And
                            UCase(MyTMProduct.Status) = "ACTIVE" And
                            ((MyTMProduct.ProductType = "ALA-CARTE-ANNUALLY") Or (MyTMProduct.ProductType = "ALA-CARTE-MONTHLY")) Then

                            'MyTMProduct.ProductType = UCase("ALA-CARTE") Then

                            '6. Checking user subscription before proceed to process TM billing termination request
                            '- check in ProductSubscription_tbl based on omg_id (ProductValidation) 
                            '- check in BillingSubscription_tbl based on tm_id (BillingValidation)                            
                            Dim ValidateProduct As New PostResponse
                            Dim ValidateBill As New PostResponse
                            ValidateProduct = MyOMGFunction.ProductValidation(GetAccountResp.LoginID, MyTMProduct.OMGProductID, True)

                            If ValidateProduct.ResponseCode = 0 Then

                                Select Case ValidateProduct.Status
                                    Case 0, 5
                                        MyResponse.ResponseCode = 106
                                        MyResponse.ResponseMsg = "NOT subscribe to product"
                                    Case 3
                                        MyResponse.ResponseCode = 106
                                        MyResponse.ResponseMsg = "Pending termination"
                                    Case 1, 4
                                        ValidateBill = MyTMFunction.BillingValidation(GetAccountResp.ServiceID, MyTMProduct.TMProductID)

                                        If ValidateBill.ResponseCode = "0" Then

                                            If ValidateBill.Status = "1" Then
                                                CalculateServiceDay = True
                                            ElseIf ValidateBill.Status = "2" Then
                                                MyResponse.ResponseCode = 106
                                                MyResponse.ResponseMsg = "Order is in process"
                                            Else
                                                MyResponse.ResponseCode = 106
                                                MyResponse.ResponseMsg = "Subscription for product " & MyTMProduct.TMProductCode & " NOT EXIST"
                                            End If
                                        Else
                                            MyResponse.ResponseCode = 101
                                            MyResponse.ResponseMsg = "Technical error - BillingValidation"
                                            ErrorMessage = "BillingValidation: " & ValidateBill.ResponseMsg & "(" & ValidateBill.ResponseCode & ")"
                                        End If
                                    Case Else
                                        MyResponse.ResponseCode = 101
                                        MyResponse.ResponseMsg = "Technical error"
                                        ErrorMessage = "ProductValidation Status: " & ValidateProduct.Status
                                End Select
                            Else
                                MyResponse.ResponseCode = 101
                                MyResponse.ResponseMsg = "Technical error - ProductValidation"
                                ErrorMessage = "ProductValidation: " & ValidateProduct.ResponseMsg & "(" & ValidateProduct.ResponseCode & ")"
                            End If

                            '7. Calculate service day
                            If CalculateServiceDay = True Then

                                If (ValidateBill.TM_BillCycle = "0") And
                                    (ValidateBill.OSMActivationDate <> "0") Then

                                    'Condition B: Order from SDP, terminate from OTT
                                    '- Need to trigger ValidateAccountId() to get TM_BillCycle
                                    Dim ValidateUserReq As New INT_ValidateUserReq
                                    Dim ValidateUserResp As New INT_ValidateUserResp

                                    ValidateUserReq.TM_AccountID = GetAccountResp.ServiceID
                                    ValidateUserReq.TM_AccountType = GetAccountResp.AccountType
                                    ValidateUserResp = MyTMFunction.ValidateAccountId(ValidateUserReq)

                                    If ValidateUserResp.ResponseCode = "0" And UCase(ValidateUserResp.Status) = "ACTIVE" Then
                                        ServiceDay = MyOMGFunction.CalculateServiceDay(MyTMProduct.ProductType, MyTMProduct.TMProductName, ValidateBill.OSMActivationDate, Integer.Parse(ValidateUserResp.TM_BillCycle))

                                        BillCycleDate = ValidateUserResp.TM_BillCycle
                                    Else
                                        ErrorMessage = "TM: Failed to get TM_BillingCycleDate"
                                    End If

                                ElseIf (ValidateBill.TM_BillCycle <> "0") And
                                        (ValidateBill.OSMActivationDate = "0") Then

                                    Dim activateDate As String = ValidateProduct.ActivationDate.ToString("MM/dd/yyyy")

                                    'Condition C: Order from OTT, terminate from OTT
                                    'Condition D: Order from OTT, terminate from SDP
                                    ServiceDay = MyOMGFunction.CalculateServiceDay(MyTMProduct.ProductType, MyTMProduct.TMProductName, activateDate, Integer.Parse(ValidateBill.TM_BillCycle))
                                Else
                                    ServiceDay = 0
                                End If

                                If ServiceDay <> 0 Then
                                    ProceedTermination = True
                                Else
                                    MyResponse.ResponseCode = 106
                                    MyResponse.ResponseMsg = "Service day is NULL"
                                End If
                            End If

                            If ProceedTermination = True Then

                                '7. Send termination to NOVA / ICP
                                Dim TempDate As DateTime = DateTime.Now
                                Dim Format As String = "MM/dd/yyyy"
                                Dim DeactivationDate As String = TempDate.ToString(Format)

                                Dim TMOrderReq As New INT_NewOrderReq
                                Dim TMOrderResp As New INT_NewOrderResp

                                '-- Need to enable when production -----------------------------
                                TMOrderReq.ReqType = "Terminate"
                                TMOrderReq.TM_AccountID = GetAccountResp.ServiceID
                                TMOrderReq.TM_AccountType = GetAccountResp.AccountType
                                TMOrderReq.TMProductID = MyTMProduct.TMProductID
                                TMOrderReq.TMProductCode = MyTMProduct.TMProductCode
                                TMOrderReq.TMProductName = MyTMProduct.TMProductName
                                TMOrderReq.ProductType = MyTMProduct.ProductType
                                TMOrderReq.DeactivationDate = DeactivationDate

                                TMOrderResp = MyTMFunction.NewOTTOrder(TMOrderReq)
                                '---------------------------------------------------------------

                                'TMOrderResp.ResponseCode = "0"     -- For TESTING ONLY!!

                                If TMOrderResp.ResponseCode = "0" Then

                                    Dim BillTerminateResp As New PostResponse

                                    If (ValidateBill.TM_BillCycle = "0") Then
                                        BillTerminateResp = MyTMFunction.BillingTermination(GetAccountResp.ServiceID, MyTMProduct.TMProductID, "Terminate", BillCycleDate)
                                    ElseIf ValidateBill.TM_BillCycle <> "0" Then
                                        BillTerminateResp = MyTMFunction.BillingTermination(GetAccountResp.ServiceID, MyTMProduct.TMProductID, "terminate")
                                    End If

                                    If BillTerminateResp.ResponseCode = "0" Then

                                        If BillTerminateResp.Status = "1" Then

                                            Dim ProdTerminateResp As New PostResponse
                                            ProdTerminateResp = MyOMGFunction.ProductTermination(MyTMProduct.OMGProductID, GetAccountResp.LoginID, "inactive", "client", ServiceDay)

                                            If ProdTerminateResp.ResponseCode = "0" And ProdTerminateResp.Status = "1" Then

                                                Dim FormatTemp As String = "dd-MM-yyyy HH:mm:ss"
                                                Dim TempDeactive As DateTime = ProdTerminateResp.TerminationDate
                                                Dim TerminationDate As String = TempDeactive.ToString(FormatTemp)

                                                Dim TempServiceEndDate As DateTime = ProdTerminateResp.ServiceEndDate
                                                Dim ServiceEndDate As String = TempServiceEndDate.ToString(FormatTemp)

                                                MyResponse.OTT_ID = MyRequest.OTT_ID
                                                MyResponse.TM_AccountID = GetAccountResp.LoginID
                                                MyResponse.ProductID = MyRequest.ProductID
                                                MyResponse.OTT_TerminationDate = TerminationDate
                                                MyResponse.OTT_ServiceEndDate = ServiceEndDate
                                                MyResponse.ResponseCode = 0
                                                MyResponse.ResponseMsg = "Successful Transaction"
                                            Else
                                                MyResponse.ResponseCode = 106
                                                MyResponse.ResponseMsg = "Failed termination"
                                                ErrorMessage = "ProductTermination Status: " & ProdTerminateResp.Status
                                            End If
                                        Else
                                            MyResponse.ResponseCode = 101
                                            MyResponse.ResponseMsg = "Technical error - Stored prod bill termination"
                                            ErrorMessage = "BillingTermination Status: " & BillTerminateResp.Status
                                        End If
                                    Else
                                        MyResponse.ResponseCode = 101
                                        MyResponse.ResponseMsg = "Technical error"
                                        ErrorMessage = "BillingTermination: " & BillTerminateResp.ResponseMsg & "(" & BillTerminateResp.ResponseCode & ")"
                                    End If
                                Else
                                    MyResponse.ResponseCode = 106
                                    MyResponse.ResponseMsg = "Failed termination to TM billing"
                                    'NOTES: Exact error please refer to TMBillingAPILog.
                                End If
                            End If

                        Else
                            Select Case MyTMProduct.ResponseCode
                                Case 0, 1       'INACTIVE, NOT EXIST, Product = BUNDLE / OTC
                                    MyResponse.ResponseCode = 103
                                    If MyTMProduct.ProductType <> ((MyTMProduct.ProductType = "ALA-CARTE-ANNUALLY") Or (MyTMProduct.ProductType = "ALA-CARTE-MONTHLY")) Then
                                        MyResponse.ResponseMsg = "Product cannot be terminate from OTT"
                                    Else
                                        MyResponse.ResponseMsg = "Product ID not exist"
                                    End If
                                Case 2
                                    MyResponse.ResponseCode = 101
                                    MyResponse.ResponseMsg = "Technical error - product"
                                    ErrorMessage = "TM: " & MyTMProduct.ResponseMsg
                                Case Else
                                    MyResponse.ResponseCode = 101
                                    MyResponse.ResponseMsg = "Technical error"
                                    ErrorMessage = "TM: " & MyTMProduct.ResponseMsg & "(" & MyTMProduct.ResponseCode & ")"
                            End Select
                        End If
                    Else
                        MyResponse.ResponseCode = 104
                        MyResponse.ResponseMsg = "Invalid TM_AccountID"
                    End If
                Else
                    MyResponse.ResponseCode = "100"
                    MyResponse.ResponseMsg = "Signature does not match"
                End If
            Else
                MyResponse.ResponseCode = 105
                MyResponse.ResponseMsg = "Missing parameter"
            End If
        Else
            Select Case MyMerchant.ResponseCode
                Case 0, 1       'INACTIVE & NOT EXIST
                    MyResponse.ResponseCode = 102
                    MyResponse.ResponseMsg = "Invalid OTT ID"
                Case 2
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg
                Case Else
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error - " & MyMerchant.ResponseCode
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg & "(" & MyMerchant.ResponseCode & ")"
            End Select
        End If

        Dim DataToPost As String = MyOMGFunction.GetSerializeXML(MyRequest, MyRequest.GetType)
        Dim MyResponseString As String = MyOMGFunction.GetSerializeXML(MyResponse, MyResponse.GetType)

        APILog.TM_AccountID = MyRequest.TM_AccountID
        APILog.OMGProductID = MyTMProduct.OMGProductID
        APILog.EventName = "OTT_Termination"
        APILog.DataReq = DataToPost
        APILog.DataResp = MyResponseString

        If MyResponse.ResponseCode = "101" Or
            (MyResponse.ResponseCode <> "0" And ErrorMessage <> "") Then
            APILog.ProcessStatus = ErrorMessage

        ElseIf MyResponse.ResponseCode <> "0" Then
            APILog.ProcessStatus = MyResponse.ResponseCode & " - " & MyResponse.ResponseMsg
        End If
        MyOMGFunction.OMGAPILog(APILog)

        Return MyResponse
    End Function


End Class
