﻿<%@ WebService Language="VB" Class="CDRSystem" %>

Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web.Services.Description
Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography
Imports System.Net
Imports System.Xml.Serialization
Imports System.Xml
Imports System.Xml.Schema

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://omg.hypp.tv/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
Public Class CDRSystem
    Inherits System.Web.Services.WebService
    
    Dim MyFunction As New CDR_Function
    Public WS_HEIS As New HEIS.HEISService
    
    <WebMethod(Description:="To process CDR.")> _
    Public Function OMG_ProcessCDR() As OMG_ValidateResp
        
        Dim MyResponse As New OMG_ValidateResp
        
        MyResponse = MyFunction.ProcessCDR()
        
        Return MyResponse
    End Function
    
    <WebMethod(Description:="To process Jumbo Junior Campaign .")> _
    Public Function OMG_ProcessJumboJunior() As String
        
        Dim MyDb As New dbConn
        Dim strConn As New SqlConnection(MyDb.connString)
        
        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If
           
            Dim TotalRecord As Integer = 0
            Dim Status As Integer = 9
            Dim ProcessStatus As String = "Successful Add JJ product"
            
            Dim DeveloperId As String = "Adm1N"
            Dim DeveloperPassword As String = "Dev4pp2016"
            Dim b4hashstringHEIS As String = "&&" + DeveloperId + "##" + DeveloperPassword + "&&"
            Dim MySignatureHEIS As String = MyFunction.GetEncryption(b4hashstringHEIS)
            
            Dim SubscribeToReq As New HEIS.SubscribeNewHEISReq
            Dim SubscribeToResp As New HEIS.SubscribeNewHEISResp
            
            Dim oRSItem As SqlDataReader
            Dim cmdList As New SqlCommand("JumboJuniorCampaignGetList", strConn)
            cmdList.CommandType = CommandType.StoredProcedure
            
            oRSItem = cmdList.ExecuteReader
            
            If oRSItem.HasRows Then
                
                Do While oRSItem.Read()
                    
                    TotalRecord = TotalRecord + 1
                
                    SubscribeToReq.DeveloperID = DeveloperId
                    SubscribeToReq.OrderType = 100
                    SubscribeToReq.UserName = oRSItem("subscriberID")
                
                    If oRSItem("subscriberID").Contains("@iptv") = True Then
                        SubscribeToReq.ProductID = "214"
                    Else
                        SubscribeToReq.ProductID = "INT11111"
                    End If
                
                    SubscribeToReq.SequenceID = "JJC" & TotalRecord
                    SubscribeToReq.Signature = MySignatureHEIS
                    SubscribeToResp = WS_HEIS.Subscription_New(SubscribeToReq)
                
                    If SubscribeToResp.ResponseCode <> "150100" Then
                        Status = 10
                        ProcessStatus = "Exception Add JJ product (" & DateTime.Now.ToString("dd-MM-yyyy") & ") - " & SubscribeToResp.ResponseMsg & "(" & SubscribeToResp.ResponseCode & ")"
                    End If
                
                    Dim cmdUpdate As New SqlCommand("JumboJuniorCampaignUpdate", strConn)
                    cmdUpdate.CommandType = CommandType.StoredProcedure
                    cmdUpdate.Parameters.Add("@SubscriberID", SqlDbType.VarChar, 50).Value = oRSItem("subscriberID")
                    cmdUpdate.Parameters.Add("@Status", SqlDbType.Int).Value = Status
                    cmdUpdate.Parameters.Add("@ProcessStatus", SqlDbType.VarChar, -1).Value = ProcessStatus
          
                    cmdUpdate.ExecuteNonQuery()
                Loop
                
                Return TotalRecord
            Else
                'NO Data - Do nothing
            End If
            
            oRSItem.Close()
            
        Catch ex As Exception
            'Do Nothing
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return 0
    End Function

End Class
